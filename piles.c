/**
 * @file grille.c
 * @author Damien BERNEAUX
 * @author Kevin COCCHI
 * @author Félix GOUEDARD
 * @author Van Man NGUYEN
 * @brief Implémentation de piles.h
 */

#include <stdlib.h>
#include <stdio.h>

#include "piles.h"


/**
 * @fn int est_vide(pile p)
 * @param p : pile.
 * @return 1 si la pile est vide, 0 sinon.
 * @brief Indique si la pile est vide ou non.
 */
int est_vide(pile p) {
    return NULL == p;
}


/**
 * @fn void detruire(pile *p)
 * @param p : pointeur vers une pile.
 * @brief Détruit la pile en libérant la mémoire associée.
 */
void detruire(pile *p) {
    pile tmp;

    while(!est_vide(*p)) {
        tmp = (*p);
        (*p) = (*p)->next;
        free(tmp);
    }

    *p = NULL;
}


/**
 * @fn void empiler(char c, pile *p)
 * @param c : couleur à insérer dans la pile.
 * @param p : pile.
 * @brief Empile la couleur c à la pile.
 */
void empiler(char c, pile *p) {
    pile res = (pile) malloc(sizeof(struct maillon));
    res->couleur = c;
    res->next = *p;
    *p = res;
}


/**
 * @fn char depiler(pile *p)
 * @param p : pile.
 * @return La couleur venant d'être dépilée.
 * @brief Dépile le sommet de la pile pointée et renvoie la valeur.
 */
char depiler(pile *p) {
    char res = ' ';

    if(!est_vide(*p)) {
        pile tmp = *p;
        res = (*p)->couleur;
        *p = (*p)->next;
        free(tmp);
    }

    return res;
}


/**
 * @fn char sommet(pile p)
 * @param p : pile.
 * @return La couleur au sommet de la pile.
 * @brief Donne la couleur qui est au sommet de la pile.
 */
char sommet(pile p) {
    if(est_vide(p))
        return ' ';
    else
        return p->couleur;
}


/**
 * @fn int hauteur(pile p)
 * @param p : pile.
 * @return La hauteur de la pile (0 si vide).
 * @brief Donne la hauteur de la pile.
 */
int hauteur(pile p) {
    int h = 0;

    while(!est_vide(p)) {
        h++;
        p = p->next;
    }

    return h;
}


/**
  * @fn void affiche_pile(pile p)
  * @param p : pile.
  * @brief Affiche le contenu de la pile, en partant du sommet.
  */
void affiche_pile(pile p) {
    while(!est_vide(p)) {
        printf("[%c] -> ", p->couleur);
        p = p->next;
    }

    printf("[]");
}
