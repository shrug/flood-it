/**
 * @file game-gui.c
 * @author Damien BERNEAUX
 * @author Kevin COCCHI
 * @author Félix GOUEDARD
 * @author Van Man NGUYEN
 * @brief Implémentation de game-gui.h
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "game-gui.h"
#include "grille.h"
#include "piles.h"
#include "solveur.h"

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

int CASE_SIZE = 25; /*!< Taille d'une case pour l'affichage graphique. */
int DECALAGE = 35; /*!< Décalage en hauteur pour afficher le nombre de coups. */


/**
 * @fn int check_victoire(grille g, int nb_tours, int nb_max_tours)
 * @param g : la grille de jeu.
 * @param nb_tours : le nombre de tours déjà joués.
 * @param nb_tours_maxi : le nombre maximal de tours pour cette partie.
 * @return 0 si le joueur a gagné, 1 si la partie continue et 2 si le joueur a perdu.
 * @brief Détermine à chaque tour si le jeu continue ou s'arrête.
 */
int check_victoire(grille g, int nb_tours, int nb_tours_maxi) {
    if(onecolor_grille(g) == 1)
        return 0;
    else {
        if(nb_tours == nb_tours_maxi)
            return 2;
        else
            return 1;
    }
}


/**
 * @fn SDL_Rect create_rectangle(int x, int y, int w, int h)
 * @param x : la position x du rectangle.
 * @param y : la position y du rectangle.
 * @param w : la longueur du rectangle.
 * @param h : la hauteur du rectangle.
 * @return Renvoye le SDL_Rect associé au rectangle tout juste créée.
 * @brief Permet de créer un rectangle qui sera dessiné sur l'interface graphique.
 */
SDL_Rect create_rectangle(int x, int y, int w, int h) {
    SDL_Rect rect;
    rect.x = x;
    rect.y = y;
    rect.h = h;
    rect.w = w;

    return rect;
}


/**
 * @fn void set_rectangle_color(SDL_Renderer *renderer, SDL_Rect *rect, int r, int g, int b, int a)
 * @param renderer : le SDL_Renderer à utiliser pour l'affichage.
 * @param rect : le rectangle dont on doit définir la couleur.
 * @param r : la composante rouge de la couleur à affecter au rectangle.
 * @param g : la composante verte de la couleur à affecter au rectangle.
 * @param b : la composante bleue de la couleur à affecter au rectangle.
 * @param a : la composante alpha de la couleur à affecter au rectangle.
 * @brief Permet de définir les couleurs d'un rectangle à dessiner sur l'interface.
 */
void set_rectangle_color(SDL_Renderer *renderer, SDL_Rect *rect, int r, int g, int b, int a) {
    SDL_SetRenderDrawColor(renderer, r, g, b, a); // Set render color
    SDL_RenderFillRect(renderer, rect); // Render rect
}


/**
 * @fn void display_text(SDL_Renderer *renderer, TTF_Font *font, char *message, int x, int y, int r, int g, int b, int a)
 * @param renderer : le SDL_Renderer à utiliser pour l'affichage.
 * @param font : la police à utiliser pour afficher le texte.
 * @param message : le message à afficher.
 * @param x : la position x du texte.
 * @param y : la position y du texte.
 * @param r : la composante rouge de la couleur à affecter au texte.
 * @param g : la composante verte de la couleur à affecter au texte.
 * @param b : la composante bleue de la couleur à affecter au texte.
 * @param a : la composante alpha de la couleur à affecter au texte.
 * @brief Permet d'afficher du texte sur l'interface graphique.
 */
void display_text(SDL_Renderer *renderer, TTF_Font *font, char *message, int x, int y, int r, int g, int b, int a) {
    SDL_Color textColor = { r, g, b, a };

    int texW = 0;
    int texH = 0;

    SDL_Surface* surface = TTF_RenderUTF8_Blended(font, message, textColor);
    SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer, surface);
    SDL_QueryTexture(texture, NULL, NULL, &texW, &texH);
    SDL_Rect dstrect = { x, y, texW, texH };

    SDL_RenderCopy(renderer, texture, NULL, &dstrect);

    SDL_DestroyTexture(texture);
    SDL_FreeSurface(surface);
}


/**
 * @fn void fill_screen(SDL_Renderer *renderer, int r, int g, int b, int a)
 * @param renderer : le SDL_Renderer à utiliser pour l'affichage.
 * @param r : la composante rouge de la couleur à affecter à l'écran.
 * @param g : la composante verte de la couleur à affecter à l'écran.
 * @param b : la composante bleue de la couleur à affecter à l'écran.
 * @param a : la composante alpha de la couleur à affecter à l'écran.
 * @brief Permet de remplir l'affichage graphique d'une couleur unique.
 */
void fill_screen(SDL_Renderer *renderer, int r, int g, int b, int a) {
    SDL_SetRenderDrawColor(renderer, r, g, b, a); // Set render color
    SDL_RenderClear(renderer);
}


/**
 * @fn int ask_load_file(SDL_Renderer *renderer)
 * @param renderer : le SDL_Renderer à utiliser pour l'affichage.
 * @return 1 si le joueur veut charger une partie existante, 0 sinon.
 * @brief Permet à l'utilisateur de charger une partie déjà sauvegardée.
 */
int ask_load_file(SDL_Renderer *renderer) {
    SDL_Event event;
    TTF_Font* font = TTF_OpenFont("arial.ttf", 18);

    int load_file = -1;

    fill_screen(renderer, 75, 75, 75, 255);
    display_text(renderer, font, "Une partie a été trouvée.", 25, 15, 255, 255, 255, 255);

    // Charger la partie
    SDL_Rect grid12_rect = create_rectangle(25, 55, 25, 25);
    set_rectangle_color(renderer, &grid12_rect, 112, 219, 112, 255);
    display_text(renderer, font, "Charger la partie [y]", 60, 57, 255, 255, 255, 255);

    // Commencer une nouvelle partie
    SDL_Rect grid18_rect = create_rectangle(25, 105, 25, 25);
    set_rectangle_color(renderer, &grid18_rect, 255, 77, 77, 255);
    display_text(renderer, font, "Commencer une nouvelle partie [n]", 60, 107, 255, 255, 255, 255);

    SDL_RenderPresent(renderer); // On met à jour l'affichage

    while(load_file == -1) {
        SDL_WaitEvent(&event);
        switch(event.type) {
            case SDL_QUIT: // Gestion de l'évènement QUIT
                exit(0);
            break;

            case SDL_KEYDOWN: // Gestion des évènements clavier
                switch(event.key.keysym.sym) {
                    case SDLK_ESCAPE:
                        exit(0);
                    break;
                    case SDLK_y:
                        load_file = 1;
                    break;
                    case SDLK_n:
                        load_file = 0;
                    break;
                }
            break;

            case SDL_MOUSEBUTTONDOWN: // Gestion de l'événement Clic de souris
                if(event.button.button == SDL_BUTTON_LEFT)
                    if(event.button.x >= 25 && event.button.x <= 50 && event.button.y >= 55 && event.button.y <= 80)
                        load_file = 1;
                    if(event.button.x >= 25 && event.button.x <= 50 && event.button.y >= 105 && event.button.y <= 130)
                        load_file = 0;
            break;
        }
    }

    TTF_CloseFont(font);
    fill_screen(renderer, 255, 255, 255, 255);
    return load_file;
}


/**
 * @fn int pick_grid_size(SDL_Renderer *renderer)
 * @param renderer : le SDL_Renderer à utiliser pour l'affichage.
 * @return Taille choisie pour la grille.
 * @brief Permet à l'utilisateur de choisir entre les trois tailles de grille définies dans le cahier des charges.
 */
int pick_grid_size(SDL_Renderer *renderer) {
    SDL_Event event;
    TTF_Font* font = TTF_OpenFont("arial.ttf", 18);

    int size = 0;

    fill_screen(renderer, 75, 75, 75, 255);
    display_text(renderer, font, "Choisissez la taille de grille :", 25, 15, 255, 255, 255, 255);

    // Grille 12x12
    SDL_Rect grid12_rect = create_rectangle(25, 55, 25, 25);
    set_rectangle_color(renderer, &grid12_rect, 112, 219, 112, 255);
    display_text(renderer, font, "Grille 12x12 [q]", 60, 57, 255, 255, 255, 255);

    // Grille 18x18
    SDL_Rect grid18_rect = create_rectangle(25, 105, 25, 25);
    set_rectangle_color(renderer, &grid18_rect, 255, 184, 77, 255);
    display_text(renderer, font, "Grille 18x18 [s]", 60, 107, 255, 255, 255, 255);

    // Grille 24x24
    SDL_Rect grid24_rect = create_rectangle(25, 155, 25, 25);
    set_rectangle_color(renderer, &grid24_rect, 255, 77, 77, 255);
    display_text(renderer, font, "Grille 24x24 [d]", 60, 157, 255, 255, 255, 255);

    SDL_RenderPresent(renderer); // On met à jour l'affichage

    while(size == 0) {
        SDL_WaitEvent(&event);
        switch(event.type) {
            case SDL_QUIT: // Gestion de l'évènement QUIT
                exit(0);
            break;

            case SDL_KEYDOWN: // Gestion des évènements clavier
                switch(event.key.keysym.sym) {
                    case SDLK_ESCAPE:
                        exit(0);
                    break;
                    case SDLK_q:
                        size = 12;
                    break;
                    case SDLK_s:
                        size = 18;
                    break;
                    case SDLK_d:
                        size = 24;
                    break;
                }
            break;

            case SDL_MOUSEBUTTONDOWN: // Gestion de l'événement Clic de souris
                if(event.button.button == SDL_BUTTON_LEFT)
                    if(event.button.x >= 25 && event.button.x <= 50 && event.button.y >= 55 && event.button.y <= 80)
                        size = 12;
                    if(event.button.x >= 25 && event.button.x <= 50 && event.button.y >= 105 && event.button.y <= 130)
                        size = 18;
                    if(event.button.x >= 25 && event.button.x <= 50 && event.button.y >= 155 && event.button.y <= 180)
                        size = 24;
            break;
        }
    }

    TTF_CloseFont(font);
    fill_screen(renderer, 255, 255, 255, 255);
    return size;
}


/**
 * @fn int pick_difficulty(SDL_Renderer *renderer)
 * @param renderer : le SDL_Renderer à utiliser pour l'affichage.
 * @return Choix de la difficulté (et donc du nombre de couleurs).
 * @brief Permet à l'utilisateur de choisir entre les trois difficultés (nombre de couleurs jouables).
 */
int pick_difficulty(SDL_Renderer *renderer) {
    SDL_Event event;
    TTF_Font* font = TTF_OpenFont("arial.ttf", 18);

    int nb_couleurs = 0;

    fill_screen(renderer, 100, 100, 100, 255);
    display_text(renderer, font, "Choisissez la difficulté :", 25, 15, 255, 255, 255, 255);

    // Grille 12x12
    SDL_Rect easy_rect = create_rectangle(25, 55, 25, 25);
    set_rectangle_color(renderer, &easy_rect, 112, 219, 112, 255);
    display_text(renderer, font, "Facile [q]", 60, 57, 255, 255, 255, 255);

    // Grille 18x18
    SDL_Rect normal_rect = create_rectangle(25, 105, 25, 25);
    set_rectangle_color(renderer, &normal_rect, 255, 184, 77, 255);
    display_text(renderer, font, "Moyen [s]", 60, 107, 255, 255, 255, 255);

    // Grille 24x24
    SDL_Rect hard_rect = create_rectangle(25, 155, 25, 25);
    set_rectangle_color(renderer, &hard_rect, 255, 77, 77, 255);
    display_text(renderer, font, "Difficile [d]", 60, 157, 255, 255, 255, 255);

    SDL_RenderPresent(renderer); // On met à jour l'affichage

    while(nb_couleurs == 0) {
        SDL_WaitEvent(&event);
        switch(event.type) {
            case SDL_QUIT: // Gestion de l'évènement QUIT
                exit(0);
            break;

            case SDL_KEYDOWN: // Gestion des évènements clavier
                switch(event.key.keysym.sym) {
                    case SDLK_ESCAPE:
                        exit(0);
                    break;
                    case SDLK_q:
                        nb_couleurs = 4;
                    break;
                    case SDLK_s:
                        nb_couleurs = 6;
                    break;
                    case SDLK_d:
                        nb_couleurs = 8;
                    break;
                }
            break;

            case SDL_MOUSEBUTTONDOWN: // Gestion de l'événement Clic de souris
                if(event.button.button == SDL_BUTTON_LEFT)
                    if(event.button.x >= 25 && event.button.x <= 50 && event.button.y >= 55 && event.button.y <= 80)
                        nb_couleurs = 4;
                    if(event.button.x >= 25 && event.button.x <= 50 && event.button.y >= 105 && event.button.y <= 130)
                        nb_couleurs = 6;
                    if(event.button.x >= 25 && event.button.x <= 50 && event.button.y >= 155 && event.button.y <= 180)
                        nb_couleurs = 8;
            break;
        }
    }

    TTF_CloseFont(font);
    fill_screen(renderer, 255, 255, 255, 255);
    return nb_couleurs;
}


/**
 * @fn void affichage_jeu(SDL_Renderer *renderer, grille g)
 * @param renderer : le SDL_Renderer à utiliser pour l'affichage.
 * @param g : la grille de jeu.
 * @brief Fournit l'affichage graphique d'une grille de jeu.
 */
void affichage_jeu(SDL_Renderer *renderer, grille g) {
    int i, j;

    SDL_Rect* cases = calloc(g.size*g.size, sizeof(SDL_Rect));

    for(i = 0; i < g.size; i++) {
        for(j = 0; j < g.size; j++) {
            cases[i*g.size + j] = create_rectangle(CASE_SIZE*i, CASE_SIZE*j + DECALAGE, CASE_SIZE, CASE_SIZE);

            if(g.val[j][i]->color == 'R')
                set_rectangle_color(renderer, &cases[i*g.size + j], 255, 0, 0, 255);
            if(g.val[j][i]->color == 'V')
                set_rectangle_color(renderer, &cases[i*g.size + j], 0, 255, 0, 255);
            if(g.val[j][i]->color == 'B')
                set_rectangle_color(renderer, &cases[i*g.size + j], 0, 0, 255, 255);
            if(g.val[j][i]->color == 'G')
                set_rectangle_color(renderer, &cases[i*g.size + j], 150, 150, 150, 255);
            if(g.val[j][i]->color == 'M')
                set_rectangle_color(renderer, &cases[i*g.size + j], 60, 40, 40, 255);
            if(g.val[j][i]->color == 'J')
                set_rectangle_color(renderer, &cases[i*g.size + j], 250, 230, 80, 255);
            if(g.val[j][i]->color == 'P')
                set_rectangle_color(renderer, &cases[i*g.size + j], 158, 14, 64, 255);
            if(g.val[j][i]->color == 'T')
                set_rectangle_color(renderer, &cases[i*g.size + j], 64, 244, 208, 255);
        }
    }

    free(cases);
}


/**
 * @fn void affichage_coups(SDL_Renderer *renderer, TTF_Font *font, int nb_tours, int nb_tours_maxi)
 * @param renderer : le SDL_Renderer à utiliser pour l'affichage.
 * @param font : la police à utiliser pour afficher le texte.
 * @param nb_tours : le nombre de tours déjà joués.
 * @param nb_tours_maxi : le nombre maximal de tours pour cette partie.
 * @brief Fournit l'affichage graphique du nombre de coups et du nombre de coups maximal.
 */
void affichage_coups(SDL_Renderer *renderer, TTF_Font *font, int nb_tours, int nb_tours_maxi) {
    char buffer[50];
    sprintf(buffer, "Nombre de coups : %d/%d", nb_tours, nb_tours_maxi);
    display_text(renderer, font, buffer, 0, 0, 0, 0, 0, 255);
}


/**
 * @fn char get_color(grille g, int x, int y)
 * @param g : la grille de jeu.
 * @param x : la position x de la case.
 * @param y : la position y de la case.
 * @return La couleur de la grille en x et y.
 * @brief Permet de récupérer la couleur de la case de la grille correspondant aux coordonnées fournies.
 */
char get_color(grille g, int x, int y) {
    if(x > 0 && (y-DECALAGE) > 0 && x/CASE_SIZE < g.size && (y-DECALAGE)/CASE_SIZE < g.size)
        return g.val[(y-DECALAGE)/CASE_SIZE][x/CASE_SIZE]->color;
    return ' ';
}


/**
 * @fn void game_loop()
 * @brief Boucle principale de jeu.
 */
void game_loop() {
    SDL_Event event; // Variables stockant les évènements SDL
    SDL_Window *window = NULL;

    SDL_Init(SDL_WINDOW_SHOWN);
    TTF_Init();

    // Création d'une fenêtre graphique
    window = SDL_CreateWindow(
        "flood-it",              // title
        SDL_WINDOWPOS_UNDEFINED, // x
        SDL_WINDOWPOS_UNDEFINED, // y
        500,                     // w
        200,                     // h
        SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL
    );

    TTF_Font *font = TTF_OpenFont("arial.ttf", 24); // Chargement de la police
    SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED); // Renderer SDL

    /* ---------------------------------------------------------------------- */

    int continuer = 1;
    int grille_init = 0;

    grille g; // Grille de jeu
    int N = 0; // Taille de la grille à créer
    int nb_couleurs_choisies = 6; // Nombre de couleurs jouables
    int nb_tours = 0; // Nombre de tours
    int nb_tours_maxi = 0; // Nombre de coups maximal

    char ancienne_couleur = 0; // Couleur sélectionnée sur le tour précédent
    char couleur_selectionnee = 0; // Couleur sélectionnée par l'utilisateur sur ce tour

    /* ---------------------------------------------------------------------- */

    FILE* fp = fopen("sauvegarde.txt", "r");
    if(fp != NULL) {  // Un fichier de sauvargarde a été trouvé !
        grille_init = ask_load_file(renderer);
        if(grille_init) { // Le joueur veut continuer sa partie
            g = file_grille("sauvegarde.txt", &nb_tours, &nb_tours_maxi, &nb_couleurs_choisies);
            N = g.size;
        }
        fclose(fp);
    }

    if(!grille_init) {  // Si aucune grille n'a encore été générée
        N = pick_grid_size(renderer); // Taille de la grille sélectionnée par l'utilisateur
        nb_couleurs_choisies = pick_difficulty(renderer); // Nombre de couleurs choisies par l'utilisateur
        g = random_grille(N, nb_couleurs_choisies); // Création d'une grille aléatoire de taille N

        pile solution = NULL;
        solveur(g, &solution, 0, nb_couleurs_choisies);
        nb_tours_maxi = hauteur(solution); // Nombre de coups maximal
        detruire(&solution);
    }

    /* ---------------------------------------------------------------------- */

    SDL_SetWindowSize(window, N*CASE_SIZE, N*CASE_SIZE + DECALAGE); // On redimensionne la fenêtre
    fill_screen(renderer, 255, 255, 255, 255);
    affichage_coups(renderer, font, nb_tours, nb_tours_maxi); // Affichage du nombre de coups
    affichage_jeu(renderer, g); // Affichage du jeu

    // Boucle principale du jeu
    while(continuer == 1) {
        continuer = check_victoire(g, nb_tours, nb_tours_maxi); // Vérification de la victoire

        // Gestion des événements
        SDL_WaitEvent(&event);
        switch(event.type) {
            case SDL_QUIT: // Gestion de l'évènement QUIT
                continuer = 3;
            break;

            case SDL_KEYDOWN: // Gestion des évènements clavier
                switch(event.key.keysym.sym) {
                    case SDLK_ESCAPE:
                        continuer = 3;
                    break;
                    case SDLK_b:
                        couleur_selectionnee = 'B';
                    break;
                    case SDLK_v:
                        couleur_selectionnee = 'V';
                    break;
                    case SDLK_r:
                        couleur_selectionnee = 'R';
                    break;
                    case SDLK_j:
                        couleur_selectionnee = 'J';
                    break;
                    case SDLK_m:
                        couleur_selectionnee = 'M';
                    break;
                    case SDLK_g:
                        couleur_selectionnee = 'G';
                    break;
                    case SDLK_p:
                        couleur_selectionnee = 'P';
                    break;
                    case SDLK_t:
                        couleur_selectionnee = 'T';
                    break;
                }
            break;

            case SDL_MOUSEBUTTONDOWN: // Gestion de l'événement Clic de souris
                if(event.button.button == SDL_BUTTON_LEFT)
                    couleur_selectionnee = get_color(g, event.button.x, event.button.y);
            break;
        }

        if(couleur_selectionnee != ancienne_couleur && couleur_valide(couleur_selectionnee, nb_couleurs_choisies)) {
            change_connexe(g, couleur_selectionnee); // Application des transformations de la grille
            nb_tours++; // Un tour vient de passer
            save_grille(g, nb_tours, nb_tours_maxi, nb_couleurs_choisies); // Sauvegarde de la grille

            ancienne_couleur = couleur_selectionnee; // On met à jour l'ancienne couleur
            fill_screen(renderer, 255, 255, 255, 255);
            affichage_coups(renderer, font, nb_tours, nb_tours_maxi); // Affichage du nombre de coups
        }

        affichage_jeu(renderer, g); // Affichage du jeu
        SDL_RenderPresent(renderer); // On met à jour l'affichage
    }

    SDL_Quit();
    TTF_CloseFont(font);
    TTF_Quit();

    if(continuer == 0) {
        printf("Vous avez gagné en %d coups. Félicitations !\n\n", nb_tours);
        remove("sauvegarde.txt");
    }
    if(continuer == 2) {
        printf("Vous avez perdu ...\n\n");
        remove("sauvegarde.txt");
    }
    if(continuer == 3) {
        printf("La partie a été quittée.\n\n");
        save_grille(g, nb_tours, nb_tours_maxi, nb_couleurs_choisies); // Sauvegarde de la grille
    }

    dealloc_grille(&g);
}


int main(int argc, char** argv) {
    (void) argc;
    (void) argv;

    game_loop();
    return 0;
}
