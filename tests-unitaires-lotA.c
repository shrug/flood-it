/**
 * @file tests-unitaires-lotA.c
 * @author Damien BERNEAUX
 * @author Kevin COCCHI
 * @author Félix GOUEDARD
 * @author Van Man NGUYEN
 * @brief Fichier contenant les tests unitaires des fonctions du lot A.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <CUnit/CUnit.h>
#include <CUnit/Basic.h>

#include "grille.h"



/**
 * @fn void change_connexe_legacy(grille g, char c)
 * @param g : la grille de jeu.
 * @param c : la couleur de remplacement.
 * @brief Change la couleur des cases de la composante 4-connexe en la couleur souhaitée.
 */
void change_connexe_legacy(grille g, char c) {
    identify_composante(g); /* on identifie la composante connexe */
    int i, j;

    for(i = 0; i < g.size; i++)
        for(j = 0; j < g.size; j++)
            if(g.val[i][j]->connexe == 1)
                change_color(g, i, j, c);
}


/**
 * @fn grille file_grille_legacy(char* path)
 * @param *path : fichier à lire.
 * @return Grille générée à partir d'un fichier.
 * @brief Génère une grille à partir d'un fichier.
 * @warning Le fichier doit avoir la forme suivante : 1 (une) ligne ne contenant que la taille de la grille en nombre, ensuite chaque ligne du tableau est délimitée par un retour à la ligne. Chaque ligne doit avoir exactement le bon nombre d'éléments (égal au nombre défini dans la première ligne). Chaque couleur doit être séparée par un espace. Le fichier ne doit pas se terminer avec un retour à la ligne.
 */
grille file_grille_legacy(char* path) {
    FILE* fp = NULL;

    // Ouverture du fichier
    fp = fopen(path, "r");
    if(fp == NULL) {
        fprintf(stderr, "Erreur lors de l'ouverture du fichier: %s\n",strerror(errno));
        exit(1);
    }

    // Lecture de la taille de la grille à créer
    char line[4];
    fgets(line, sizeof(line), fp);
    int n = atoi(line);

    grille g = alloc_grille(n); // Création de la grille

    char buf[2];  // Buffer pour la lecture du caractère
    int i = 0, j = 0;

    // Lecture d'un caractère tant qu'on n'est pas à la fin
    while(fgets(buf,sizeof(buf),fp)!=NULL) {

        switch(buf[0]) {
            case '\n':
                i++;

                if(i >= n) { // Vérification du nombre de lignes
                    fprintf(stderr, "Dimensions invalides.\n");
                    dealloc_grille(&g);   // Échec de la création, libérer la mémoire avant de quitter
                    exit(1);
                }
                if(j != n){ // Vérification du nombre d'éléments dans la ligne
                    fprintf(stderr, "Dimensions invalides : %d;%d.\n",i,j);
                    dealloc_grille(&g);
                    exit(1);
                }
                j = 0;
            break;

            case ' ':
            break;

            case 'B':
            case 'V':
            case 'R':
            case 'J':
            case 'M':
            case 'G':
                if(j >= n) { // Vérifications de dimensions
                    fprintf(stderr, "Dimensions invalides : %d;%d.\n",i,j);
                    dealloc_grille(&g);
                    exit(1);
                }
                change_color(g, i, j, buf[0]);
                j++;
            break;

            default:
                fprintf(stderr, "Erreur : couleur invalide : %c, (%d;%d)\n", buf[0],i,j);
                dealloc_grille(&g);
                exit(1);
        }
    }

    // On doit vérifier que la boucle ne s'est pas terminée à cause d'une erreur
    if(!feof(fp)){
        fprintf(stderr, "Erreur, fin de lecture prématurée: %d;%d\n",i,j);
        dealloc_grille(&g);
        exit(1);
    }
    if(i < n-1) { // Vérification du nombre de lignes
        fprintf(stderr, "Dimensions invalides.\n");
        dealloc_grille(&g);   // Échec de la création, libérer la mémoire avant de quitter
        exit(1);
    }

    fclose(fp);
    return g;
}


/**
 * @fn test_equals_grilles_vide(void)
 * @brief Test : génération d'une grille vide de taille n
 */
void test_equals_grilles_vide(void) {
    grille g3 = alloc_grille(3);
    grille g4 = alloc_grille(4);
    grille g5 = alloc_grille(5);

    CU_ASSERT(equals_grilles(g4, g4) == 1);
    CU_ASSERT(equals_grilles(g3, g5) == 0);

    dealloc_grille(&g3);
    dealloc_grille(&g4);
    dealloc_grille(&g5);
}


/**
 * @fn test_change_color(void)
 * @brief Test : changement de couleur d'une case
 */
void test_change_color(void) {
    grille test_grid = file_grille_legacy("TestsGrids/lotA-grid-etape1");
    change_color(test_grid, 5, 6, 'R');
    change_color(test_grid, 3, 2, 'B');

    CU_ASSERT(test_grid.val[5][6]->color == 'R');
    CU_ASSERT(test_grid.val[3][2]->color == 'B');

    dealloc_grille(&test_grid);
}


/**
 * @fn test_onecolor_grille(void)
 * @brief Test : vérification de l'unicité de la couleur dans la grille
 */
void test_onecolor_grille(void) {
    grille one_color_true = file_grille_legacy("TestsGrids/one-color-true");
    grille one_color_false = file_grille_legacy("TestsGrids/one-color-false");

    CU_ASSERT(onecolor_grille(one_color_true));
    CU_ASSERT(!onecolor_grille(one_color_false));

    dealloc_grille(&one_color_true);
    dealloc_grille(&one_color_false);
}


/**
 * @fn test_equals_grilles_file(void)
 * @brief Test : génération d'une grille depuis un fichier
 */
void test_equals_grilles_file(void) {
    grille testgrid = file_grille_legacy("TestsGrids/test-grid");
    grille testgrid2 = file_grille_legacy("TestsGrids/test-grid2");

    CU_ASSERT(equals_grilles(testgrid, testgrid) == 1);
    CU_ASSERT(equals_grilles(testgrid, testgrid2) == 0);

    dealloc_grille(&testgrid);
    dealloc_grille(&testgrid2);
}


/**
 * @fn test_change_connexe
 * @brief  Test : changement de couleur de la composante connexe
 */
void test_change_connexe(void) {
    int i;
    char i_to_str[2];
    char filename[50];

   char changement_connexe[15] = {'J', 'B', 'V', 'R', 'B', 'M', 'J', 'G', 'V', 'B', 'R', 'M', 'J', 'B', 'R'};

   /* A chaque itération, on ouvre le fichier de l'étape en cours
      On applique le changement de couleur qu'on a opéré sur les fichiers
      On vérifie que le résultat est bien celui attendu dans les fichiers */
    for(i = 1; i <= 15; i++) {
        /* Fichier de la grille correspondant à l'état actuel */
        strcpy(filename, "TestsGrids/lotA-grid-etape");
        sprintf(i_to_str, "%d", i);
        strcat(filename, i_to_str);
        grille to_next_etape = file_grille_legacy(filename);
        /* Changement de couleur de la composante */
        change_connexe_legacy(to_next_etape, changement_connexe[i-1]);

        /* Fichier de la grille correspondant au résultat attendu */
        strcpy(filename, "TestsGrids/lotA-grid-etape");
        sprintf(i_to_str, "%d", i+1);
        strcat(filename, i_to_str);
        grille attendu = file_grille_legacy(filename);

        CU_ASSERT(equals_grilles(attendu, to_next_etape) == 1); /* Test */

	dealloc_grille(&to_next_etape);
	dealloc_grille(&attendu);
    }
}


/**
 * @fn test_all(void)
 * @brief Test : effectue tous les tests
 */
int test_all() {
    CU_pSuite pSuite = NULL;

    if(CU_initialize_registry() != CUE_SUCCESS) {
        return CU_get_error();
    }

    pSuite = CU_add_suite("Suite", NULL, NULL);

    if(pSuite == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    if(CU_add_test(pSuite, "Test d'égalité des grilles allouées vides", test_equals_grilles_vide) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    if(CU_add_test(pSuite, "Test de l'unicité de la couleur dans la grille", test_onecolor_grille) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    if(CU_add_test(pSuite, "Test de changement de couleur d'une case", test_change_color) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    if(CU_add_test(pSuite, "Test d'égalité des grilles allouées à partir d'un fichier", test_equals_grilles_file) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    if(CU_add_test(pSuite, "Test de changement de couleur d'une composante connexe", test_change_connexe) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();
    CU_cleanup_registry();

    return 0;
}


int main() {
    test_all();
    return 0;
}
