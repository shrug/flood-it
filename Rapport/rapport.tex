\documentclass[11pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[top=3cm,bottom=3cm]{geometry}

\usepackage[frenchb]{babel}
\frenchbsetup{StandardLists=true}

\usepackage{amsmath}

\usepackage{fancyhdr}
\pagestyle{fancy}
\usepackage{lastpage}

\usepackage{pdflscape}

\usepackage{fancyvrb}

\usepackage{graphicx}
\usepackage{tikz,pgf}
\usepackage{mathrsfs}
\usepackage{caption}
\usetikzlibrary{arrows}

\title{Projet informatique\\Color Flood}
\author{Damien BERNEAUX\\Kévin COCCHI\\Félix GOUEDARD\\Van Man NGUYEN\\ \\ Groupe \texttt{/shrug}}

\usepackage{hyperref}
\hypersetup{
  colorlinks,
  citecolor=black,
  filecolor=black,
  linkcolor=black,
  urlcolor=black
  }

\fancyhead[C]{}
\fancyhead[L]{Projet informatique}
\fancyhead[R]{Color Flood - Groupe /shrug}

\fancyfoot[R]{\thepage /\pageref{LastPage}}
\fancyfoot[C]{}
\fancyfoot[L]{\hyperref[sommaire]{Retour au sommaire}}

\begin{document}

\begin{titlepage}
	\centering
	\vspace*{3cm}
	{\scshape\huge Projet Informatique\par}
	\vspace{0.5cm}
	{\Huge\bfseries Color Flood\par}
	\vspace{4cm}
	{\LARGE\bfseries Groupe /shrug\par}
	\vspace{1cm}
	{\Large Damien BERNEAUX\\Kévin COCCHI\\Félix GOUEDARD\\Van Man NGUYEN\par}
	\vspace{1cm}
	{\footnotesize \today\par}
\end{titlepage}

\tableofcontents
\thispagestyle{empty}
\label{sommaire}

\vspace{5cm}

\newpage
\setcounter{page}{1}

\part{Lot A}
\section{Travail préambulaire}
\subsection{Conception}
\par \emph{ColorFlood} est un jeu informatique faisant l'objet d'un projet par groupe de quatre personnes. Le but est de recouvrir une grille carré d'une seule couleur, en partant d'une grille composée de 6 couleurs réparties aléatoirement sur la surface. Pour cela, le joueur devra partir du coin supérieur gauche, et choisir une des couleurs adjacentes à cette couleur de départ pour progresser dans le jeu.
\medbreak
\par Il est écrit en \texttt{C} et sera décomposé en plusieurs lots, de A à D. Ce premier lot constitue la base du jeu, à savoir la définition des structures de données, leur initialisation, leur affichage ainsi que leur gestion.
\medbreak
\par La grille de jeu sera composée de six couleurs : bleu, vert, rouge, jaune, marron et gris. À terme, la taille de la grille sera limitée à 12 cases, 18 cases ou 24 cases. Le joueur n'aura qu'un nombre de coups limité pour finir la partie.

\subsection{Organisation du travail}
\par La répartition des rôles a été réalisée à l'aide de \texttt{Trello}, site d'organisation de projet, et elle a été réalisée de la manière suivante :
\begin{itemize}
\item Damien BERNEAUX a été en charge de la création des structures de données et de la gestion de la mémoire.
\item Félix GOUEDARD a été responsable de la gestion de la composante connexe.
\item Van Man NGUYEN a géré l'affichage et la génération de la grille à partir d'un fichier, ainsi que de la documentation.
\item Kévin COCCHI, référent pour ce lot, s'est occupé de la génération des grilles de manière aléatoire, du changement de couleur pour une case, et des tests unitaires ainsi que du \texttt{makefile}.
\item L'ensemble des membres a participé à la réalisation du rapport.
\end{itemize}
\par Le travail a été effectué de façon collaborative grâce au logiciel de gestion de versions \texttt{git}, les fichiers étant stockés plus particulièrement sur GitLab et sur chacun de nos ordinateurs.

\newpage

\section{Structures de données}
\subsection{Définition des structures}
\par Nous avons, lors de notre première réunion, décidé de la structure de donnée voulue. Il a été convenu pour la grille de faire une structure comprenant :
\begin{itemize}
\item Un entier $n$ correspondant à la taille de la grille.
\item Un tableau de taille $n$ * $n$ contenant les cases. Ces-dernières sont également des structures de données définies de la manière suivante :
  \begin{itemize}
  \item Un caractère contenant la couleur de la case (B, V, R, J, M, G).
  \item Un entier valant $0$ ou $1$, indiquant si la case appartient à la composante connexe.
  \end{itemize}
\end{itemize}
\par La taille de la grille étant fixe sur le cours d'une partie, il suffira alors de réaliser les allocations mémoire nécessaires en début de partie. Cette structure permet également d'accéder rapidement au contenu des cases.

\subsection{Gestion de la mémoire}
\par À la génération ou à la destruction d'une grille, de la mémoire doit être allouée ou libérée. Ainsi, à la création d'une grille de taille $n$ * $n$, on alloue en tout $n^2$ cases qui contiendront toutes par défaut un caractère \texttt{0} comme couleur et le nombre $0$ comme indicateur d'appartenance à la composante connexe, sauf la case au coin supérieur gauche dont l'indicateur vaudra $1$.
\medbreak
\par La libération de la mémoire allouée pour la grille consiste d'abord en la libération de chacune des cases, puis ensuite de chaque ligne de la grille, pour finalement libérer la grille dans son ensemble.
\medbreak
\par La détection des fuites de mémoire a été réalisée à l'aide de \texttt{valgrind}. Des allocations de grille puis leur destruction sont effectuées, afin de vérifier que \texttt{valgrind} n'indique aucune fuite de mémoire (autant d'allocations que de libérations de mémoire).


\section{Initialisation de la grille}
\subsection{À partir de valeurs aléatoires}
\par L'initialisation d'une grille de façon aléatoire a été effectuée grâce à un tableau contenant les six couleurs possibles. Il suffit alors de choisir un entier aléatoirement pour chaque case, et de récupérer la couleur qui est associée à cet entier dans le tableau de couleurs.

\subsection{À partir d'un fichier}
\par Le choix a été fait ici d'utiliser au maximum les fonctions de la librairie standard de \texttt{C} pour la lecture de fichier, pour des raisons de portabilité et d'efficacité.
\medbreak
\par Le fichier doit également suivre un format bien précis. La première ligne du fichier ne peut contenir au maximum que trois caractères, correspondant à un nombre : la taille de la grille à générer. Ensuite, le reste du fichier ne peut que contenir des espaces, des retours à la lignes, et les caractères correspondant aux couleurs. Un retour à la ligne marque un changement de ligne dans la grille. On prend soin de vérifier que la taille de grille demandée correspond bien au nombre de lignes lues, ainsi qu'au nombre de cases sur chaque ligne du fichier.

\section{Gestion de la composante connexe}
\subsection{Identification de la composante connexe}
\par La fonction d'identification de la composante connexe \texttt{identify\_composante} est constituée de deux fonctions auxiliaires. La première, \texttt{reset\_composante}, permet de réinitialiser la composante. La seconde, \texttt{composante\_connexe}, est chargée de parcourir récursivement la grille pour ajouter à la composante connexe les cases qui doivent l'être.
\medbreak
\par La fonction \texttt{composante\_connexe} est récursive et prend en arguments les coordonnées d'une case. Si jamais une case voisine a la même couleur que la case actuellement traitée, on exécute \texttt{composante\_connexe} sur cette case voisine, et on l'ajoute à la composante connexe en mettant à $1$ l'indicateur de composante connexe.
\medbreak
\par Pour déterminer la composante connexe dans son intégralité, on fait un appel à \texttt{identify\_composante} qui va d'abord réinitialiser la composante connexe via \linebreak\texttt{reset\_composante} (on met à $0$ l'indicateur de composante connexe de toutes les cases, sauf pour la case en haut à gauche). On fait ensuite appel à \texttt{composante\_connexe} en commençant par la case en haut à gauche. La fonction \texttt{composante\_connexe} va alors parcourir récursivement l'ensemble de la grille en rajoutant à la composante connexe les cases concernées.

\subsection{Coloration de la composante connexe}
\par Pour la coloration, on identifie dans un premier temps la composante connexe en faisant appel à \texttt{identify\_composante}. Il suffit alors de parcourir la grille et de changer la couleur de chaque case appartenant à la composante connexe.

\section{Tests unitaires}
\par Pour les tests unitaires du lot A, l'objectif est de vérifier que les allocations de grille ainsi que le changement de couleur d'une case voire de la composante connexe dans son intégralité produisent bien le résultat souhaité.
\medbreak
\par Dans un premier temps, nous avons réalisé une fonction \texttt{equals\_grilles} dont l'objectif est de vérifier que deux grilles passées en argument sont bien égales. On vérifie ainsi que la couleur de chaque case correspond entre les deux grilles, et si une couleur est différente, la fonction renvoie $0$, indiquant que les grilles ne sont pas égales.
\medbreak
\par Cette fonction \texttt{equals\_grilles} a permis de tester les allocations de grilles : dans un premier temps, on vérifie que l'allocation de grilles vides fournit le même résultat si la taille est la même, et des grilles différentes si leurs tailles sont distinctes.
\medbreak
\par Dans un second temps, nous vérifions que le changement de couleur d'une case effectue bien le changement à la bonne place. Nous vérifions aussi que la vérification de l'unicité de la couleur dans une grille est correcte, en chargeant deux fichiers : l'un correspond à une grille de couleur unique, l'autre à une grille où une case possède une couleur différente du reste de la grille. On teste ainsi la fonction \texttt{onecolor\_grille} fournit le résultat souhaité.
\medbreak
\par Enfin, on vérifie la fonction de changement de couleur d'une composante connexe \texttt{change\_connexe}. Il suffit de tester cette fonction puisqu'elle fait appel à l'ensemble des autres fonctions concernant la composante connexe. Le procédé est le suivant : nous avons choisi une grille initiale, puis nous avons manuellement déduits les étapes suivantes en effectuant des changements de couleur de la composante connexe (cf. les fichiers \texttt{lotA-grid-etapeN}). Pour le test unitaire, on vérifie pour chaque fichier qu'en appliquant la fonction \texttt{change\_connexe}, le résultat obtenu pour l'étape $N$ correspond au résultat attendu pour l'étape $N+1$.

\part{Lot B}
\setcounter{section}{0}
\section{Introduction}
\par Ce lot contient les prémisses du jeu (en console uniquement pour le moment), et donc l'ensemble des fonctions qui y sont liées, telles que l'affichage des menus et des actions possibles, la lecture des entrées au clavier, ainsi que l'implémentation de la boucle de jeu. Les tâches ont été réparties comme suit :
\begin{itemize}
\item Damien a complété l'affichage des menus et a écrit les fonctions permettant au joueur de choisir une couleur.
\item Félix a écrit la boucle de jeu avec ses conditions d'arrêt.
\item Kévin s'est occupé du changement de couleur de la composante connexe suite au choix du joueur.
\item Van Man s'est chargé de la lecture des caractères.
\item Enfin, la documentation et le rapport ont été réalisés par l'ensemble du groupe.
\end{itemize}

\section{Lecture des actions}
\par La lecture des caractères se fait grâce à la fonction \texttt{read\_key} qui renvoie le code du caractère lu au clavier. Son principal attrait est la lecture des caractères à la volée. Contrairement à \texttt{scanf} et des autres fonctions de la librairie standard, il n'est pas nécessaire de valider en appuyant sur la touche \texttt{[Entrée]}. Toutefois, comme elle s'appuie sur des fonctions qui ne sont pas contenues dans la librairie standard du \texttt{C}, il est possible qu'elle génère des problèmes de compatibilité avec d'autres systèmes.

\medbreak

\par Par ailleurs, toutes les entrées sont vérifiées afin que le joueur choisisse bien une couleur valide ainsi qu'une taille de grille valide. Si, par exemple, le joueur appuie sur une touche ne correspondant à aucune couleur, le jeu lui redemandera de choisir une couleur tant que l'entrée n'est pas valide. Il en est de même pour la taille de la grille.

\section{Boucle de jeu}
\subsection{Initialisation du jeu}
\par Le jeu débute par l'affichage d'un menu invitant le joueur à choisir la taille de la grille et le nombre de coups autorisés. Ces fonctions vont inviter l'utilisateur à rentrer des valeurs tant que celles-ci ne sont pas valides.

\subsection{Boucle principale de jeu}
\par Une fois les choix effectués, la grille de la partie est générée aléatoirement selon la taille choisie, en faisant appel à la fonction correspondante écrite dans le lot A. À terme, le nombre de coups maximal sera déterminé en fonction de la grille générée. Pour le moment, le nombre de coups maximal doit être saisi manuellement par l'utilisateur.
\medbreak
\par Suite à ces étapes préliminaires, la boucle principale du jeu démarre. Le programme restera dans cette boucle tant que le joueur n'a pas gagné ou tant que le nombre de coup est inférieur au nombre autorisé.
\medbreak
\par En début de boucle, une fonction nommée \texttt{check\_victoire} détermine si la partie doit continuer en fonction du nombre de coups joués et du nombre de coups autorisés. Si la partie doit s'arrêter, le programme sort de la boucle en affichant un message de défaite, ou de victoire, selon le cas.
\medbreak
\par Du moment que la défaite ou la victoire n'a pas lieu, la partie continue et le joueur doit choisir une couleur différente de celle précédemment choisie. La composante connexe prend alors cette couleur, et s'étend à toutes les cases qui lui sont adjacentes et qui portent la même couleur. Enfin, le nombre de tours est incrémenté et une nouvelle itération de la boucle commence.

\section{Problème d'optimisation lié au lot A}
\par Nous nous sommes rendus compte en jouant une partie sur une grille de taille $18$*$18$ ou $24$*$24$ que les temps de chargement en fin de partie devenaient de plus en plus longs, dépassant même plusieurs minutes, et rendant le jeu injouable.
\medbreak
\par La cause de ces ralentissements vient de la fonction de vérification de la composante connexe. La composante connexe est en effet être remise à zéro à chaque fin de tour afin d'être recalculée, en prenant en compte les modifications de la grille. Le nombre de cases parcourues récursivement croit alors très rapidement, et c'est un problème que nous avions déjà remarqué en travaillant sur le lot A, mais que nous n'avions jamais pensé qu'il causerait de telles latences.
\medbreak
\par Une des solutions à ce problème serait de construire à chaque tour la liste des cases situées à la frontière de la composante connexe. Il s'agit ensuite d'étendre la composante connexe aux cases voisines de même couleur que la couleur choisie par l'utilisateur. Nous réaliserons son implémentation pour le lot suivant, afin de rendre le jeu jouable pour des grilles de taille plus importante.

\newpage
\part{Lot C}
\setcounter{section}{0}
\section{Introduction}
\par L'enjeu de cette partie du projet est de construire un solveur. Il s'agit d'un programme qui va, sur la donnée d'une grille, trouver une solution au jeu. Cette solution sera stockée grâce à une structure de pile. Les coups menant vers la victoire y seront empilés. Cette solution nous permettra également de déterminer le nombre maximal de coups.
\medbreak
\par De plus, un début fonctionnel d'interface graphique est proposé dans ce lot. Le développement est encore en cours et des fuites mémoires ainsi qu'une impression d'interface simpliste peut se faire ressentir. L'ensemble devrait être amélioré pour le lot D.
\medbreak
\par Le jeu propose donc à cet instant deux possibilités :
\begin{enumerate}
\item Le jeu en mode terminal, où le joueur choisit la couleur de son coup uniquement au clavier et où l'affichage peut devenir complexe à lire.
\item Le jeu en mode graphique, où le joueur a la possibilité de choisir la couleur de son coup en cliquant sur les cases de la grille, et où l'affichage facilite la lecture du jeu.
\end{enumerate}

\par Pour ce lot, le travail a été réparti de la façon suivante :
\begin{itemize}
\item Van Man NGUYEN s'est occupé de l'implémentation de la structure de piles.
\item Kévin COCCHI et Damien BERNEAUX se sont chargés de l'interface graphique.
\item Enfin, l'ensemble des quatre membres du groupes ont travaillés sur le solveur, de la programmation initiale au debug de ce dernier, mais aussi sur l'écriture du rapport et de la documentation.
\end{itemize}

\section{Les piles}
\subsection{Implémentation}
\par Nous avons fait le choix de construire notre structure de pile à l'aide de maillons chaînés. Cette implémentation nous permet d'avoir des piles non limitées en hauteur. En effet, les piles serviront à stocker les coups successifs pour arriver à une victoire, et nous ne pouvons pas à priori prévoir une taille maximale.
\medbreak
\par S'agissant d'une structure de type FIFO (First In First Out), le sommet de la pile sera le premier maillon de la chaîne tandis que la base sera le dernier maillon de la chaîne. Chaque maillon contiendra deux informations : la première, un caractère pour stocker le coup qui a été joué par le solveur, la seconde, un pointeur vers le maillon suivant. Le dernier maillon de la pile pointe vers \texttt{NULL}, signifiant ainsi qu'il n'a aucun successeur et que la fin de pile est atteinte.

\subsection{Fonctionnalités}
\par Le fichier \texttt{piles.c} inclut les fonctionnalités de base des piles. Une première fonction permet de vérifier si la pile est vide. Ensuite vient une fonction de destruction, dont le but est de vider la mémoire associée à une pile. On libère les maillons un à un de proche en proche.
\medbreak
\par On trouve ensuite une fonction de dépilement qui suit un principe similaire à celui utilisé pour vider une pile, mais à l'échelle d'un seul maillon. La fonction renverra en plus la valeur de la couleur qui vient d'être dépilée. La fonction d'empilement construit un maillon, y affecte la couleur passée en argument, et le place au sommet de la pile.
\medbreak
\par Enfin, on trouve une fonction d'affichage, une fonction retournant la valeur du sommet, et une dernière donnant la hauteur de la pile.

\section{Le solveur}
\par Le solveur a pour objectif de proposer une solution à la grille passée en argument. On se basera ensuite sur cette solution pour calculer le nombre de coups maximal autorisé. Le joueur humain aura donc pour objectif de faire aussi bien, voire mieux, que la machine. Pour l'instant, la solution retournée est la première solution trouvée et celle-ci n'est pas forcément la plus optimale.

\subsection{Déroulement}
\par Afin de préserver la grille, d'une part parce que le joueur doit faire sa partie sur la même grille, d'autre part parce qu'elle sera modifiée par référence, nous réaliserons à chaque itération une copie de la grille. Ainsi, on peut revenir à l'état de la grille au moment où le solveur a été appelé.
\medbreak
\par Une fois la copie effectuée, nous allons vérifier pour chaque couleur qu'en changeant la composante connexe de couleur, la condition de victoire pour la copie n'est pas vérifiée (autrement dit, que la grille n'est pas d'une seule couleur). On empile ainsi la couleur dans la pile solution. Bien évidemment, on peut éviter de tester le changement de composante connexe pour la couleur qui y est déjà présente ...
\medbreak
\par Si la grille n'a pas encore été complétée en jouant une couleur, on appelle récursivement une nouvelle fois le solveur avec cette nouvelle grille et la pile de solution en paramètre. Ces appels vont se suivre tant qu'une solution n'est pas trouvée, autrement dit tant que la grille n'est pas d'une couleur unique.
\medbreak
\par Si la condition de victoire est vérifiée, on arrête l'ensemble des appels récursifs. La pile solution contient alors la première suite de couleurs à jouer trouvée, bien que non nécesairement optimale. On pensera bien évidemment au fur et à mesure des appels récursifs à vider la mémoire de l'ensemble des copies de grilles réalisées.

\subsection{Limites}
\par Le solveur a pour le moment deux limites. La première concerne la solution retournée. Il s'agit de la première solution que l'algorithme va trouver, et rien ne garantit son optimalité.
\medbreak
\par La seconde est en lien avec la mémoire utilisée par le solveur. Nous créons une copie de la grille à chaque itération. Or, ces copies ne seront supprimées que lorsque la solution sera trouvée, en redescendant dans la pile des appels. Finalement, la mémoire occupée par les copies des grilles va s'accroître au fur et à mesure que nous avançons dans la résolution, jusqu'à ce que l'algorithme trouve une solution.


\section{L'interface graphique}
\par Pour réaliser une interface graphique, nous avons utilisé la bibliothèque \texttt{SDL2}. La compilation requiert donc son installation. Cette bibliothèque permet assez facilement de réaliser une véritable interface pour le jeu plutôt que la simple utilisation du terminal, avec gestion des affichages mais aussi des événements souris ou clavier.
\subsection{Affichage de la grille}
\par Pour réaliser un affichage de la grille, peu de travail a été nécessaire. Il nous a fallu disposer d'une fonction pour afficher des carrés d'une certaine couleur à une position donnée (les cases de notre grille). L'affichage de la grille consiste alors simplement à itérer sur les lignes et colonnes puis afficher un carré de la couleur associée aux bonnes coordonnées, en donnant une taille fixe à chacune des cases.
\medbreak
\par Il faut toutefois changer l'affichage de la grille à chaque tour de jeu pour refléter le changement de couleur de la composante connexe. Pour cela, on nettoie la fenêtre en la remplissant en blanc, puis on fait appel à la fonction qui permet l'affichage de la grille.
\medbreak
\par Un problème qui se pose alors est qu'un clignotement a lieu à chaque fois qu'un nouveau tour commence, ce qui est du au nettoyage de la fenêtre. Il faudra donc potentiellement dans le lot D créer l'affichage du tour suivant pendant le tour actuel et le stocker avant de l'afficher pour éviter le scintillement.
\subsection{Actions du joueur}
\par Le joueur peut choisir une couleur en pressant la touche associée sur le clavier, ou bien en sélectionnant la couleur sur la grille à l'aide de sa souris. A chaque tour, il est donc nécessaire de vérifier les actions clavier ou souris que l'utilisateur a effectuées et effectuer une action en conséquence. Chaque tour de la boucle de jeu va alors écouter ces événements.
\medbreak
\par Lorsque l'utilisateur tape une touche de son clavier, on vérifie si il a ou non tapé l'une des touches R, V, B, J, M ou G. Si c'est le cas, on réalise alors un changement de couleur de la composante connexe en vérifiant que la couleur est différent. On actualise ensuite l'affichage et on passe au tour suivant.
\medbreak
\par Lorsque le joueur clique quelque part sur la grille, il faut récupérer la couleur de la case, si tant est qu'une case se situe bien sous le curseur. Pour cela, on va juste déterminer la case de la grille en récupérant les coordonnées du curseur au moment du clic ainsi que la taille de chaque case de la grille. On change ensuite la couleur de la composant en accord avec ce choix.

\newpage
\part{Lot D}
\setcounter{section}{0}
\section{Introduction}
\par Nous avons choisi pour cette dernière partie d'améliorer les fonctionnalités déjà existantes et d'en rajouter quelques unes, dont la sauvegarde automatique. Un joueur peut ainsi suspendre sa partie à tout moment et y revenir après, dans le mode de son choix. En parallèle, nous avons constaté de forts ralentissements sur la version graphique du jeu, pour des grilles dont la taille dépasse les 18 cases de côté. Enfin, nous avons tenté d'améliorer le solveur afin que la solution retournée soit la plus petite possible.
\par La répartition des tâches s'est faite de la manière suivante :
\begin{itemize}
\item Kévin COCCHI et Damien BERNEAUX se sont occupés de l'optimisation de l'interface graphique. Ils ont également travaillé sur l'ajout d'une fonctionnalité permettant au joueur de choisir la difficulté du jeu, basée sur le nombre de couleurs sur la grille.
\item Félix GOUEDARD s'est chargé de l'implémentation du système de sauvegarde.
\item Van Man NGUYEN a travaillé sur le solveur.
\item Enfin, comme à notre habitude, le rapport a été un effort commun de toutes les forces en présence. Il en va de même pour la phase de débuggage et des dernières corrections pour éliminer des fuites de mémoire.
\end{itemize}

\section{Le solveur : état des lieux}
\par Le solveur tel qu'il est actuellement ne renvoie que la première solution trouvée, et celle-ci n'est que très rarement une solution presque optimale. La solution proposée était une structure d'arbre de grilles. Nous n'avons pas retenu cette solution en raison des choix effectués pour ce lot, notamment celui de permettre un nombre de couleurs variable, qui complexifieraient la construction, ainsi qu'en raison du manque de temps.
\medbreak
\par De plus, une telle structure prendrait la forme de nœuds composés d'une grille en donnée, et d'autant de branches qu'il n'y a de couleurs jouables. Or, plus nous voulons une solution optimale, plus nous devons construire des arbres <<profonds>>. En effet, l'idée serait de comparer, après quelques itérations, la taille des composantes connexes. La meilleure solution à un instant donné est donc celle qui nous permettra d'avoir la plus grand composante connexe dans, par exemple, cinq coups, si la partie ne se termine pas entre-temps. Évidemment, si nous trouvons une solution, celle-ci sera privilégiée.
\medbreak
\par Il nous faudrait donc calculer la taille des composantes connexes sur un grand nombre d'arbres. Ces opérations sont coûteuses en temps, mais aussi en mémoire. Une solution similaire consiste à créer des boucles \texttt{for} imbriquées. À un arbre de profondeur $k$ correspondra $k$ boucles \texttt{for} imbriquées. Nous comparons en fin de chaque boucle la taille de la composante connexe obtenue, et empilons les coups pour parvenir à cette solution si celle-ci était meilleure que la précédente.
\medbreak
\par En tentant une implémentation de cette solution pour deux boucles imbriquées, nous obtenons une solution rapidement, mais dont l'optimalité est encore moindre en moyenne que celle obtenue via le solveur initial. En rajoutant une boucle, le temps d'attente devient trop important. Par conséquent, la solution n'a pas été retenue et nous estimons que le solveur initial est suffisant.

\section{Nombre de couleurs}
\par Nous avons fait le choix de donner la possibilité au joueur de configurer la difficulté de sa partie, en modifiant le nombre de couleurs possibles dans la grille. Ainsi, le jeu est plus à même de proposer une expérience adaptée à l'utilisateur.
\medbreak
\par En premier lieu, on diversifie le panel de couleurs disponibles, en ajoutant le violet et le turquoise à la liste. Puis, on implémente le choix de la difficulté : au début de chaque nouvelle partie, on propose au joueur de choisir entre 4, 6 ou 8 couleurs pour sa grille de jeu. On laisse ensuite à l'utilisateur la possibilité de choisir les couleurs qu'il souhaite utiliser, parmi la palette des teintes disponibles.
\medbreak
\par L'implémentation de cette fonctionnalité nous a conduit à modifier plusieurs parties du programme. Il faut en effet régulièrement contrôler que le nombre total de couleurs dans la partie est le bon. On a de ce fait mis à jour plusieurs fonctions, pour qu'elles prennent en compte ce nombre, ainsi que les couleurs actuellement utilisées.

\section{Système de sauvegarde}
\subsection{Enregistrement de la partie}
\par Pour effectuer la sauvegarde, on écrit la fonction \texttt{save\_grille}. Celle-ci crée dans le répertoire courant un fichier \texttt{sauvegarde.txt} contenant les informations de la partie. Si celui-ci existe déjà, elle réécrit son contenu pour le mettre à jour. Les informations stockées dans le fichier sont :
\begin{itemize}
	\item la taille de la grille de jeu
	\item le nombre de couleurs dans cette partie
	\item le nombre de coups joués
	\item le nombre de coups maximum 
	\item la grille de jeu
\end{itemize} 
\par Cette fonction est appelée après chaque itération de la boucle de jeu, mettant régulièrement à jour la sauvegarde. Ainsi, même en cas d'interruption subite du programme, l'utilisateur garde toujours les données les plus récentes de sa dernière partie. Il est donc toujours capable de reprendre le jeu exactement où il s'est arrêté.
\subsection{Chargement de la partie}
\par On cherche maintenant à récupérer les donnés contenues dans \texttt{sauvegarde.txt}. Pour cela, on modifie la fonction permettant d'initialiser une grille à partir d'un fichier, en adaptant le format que doit suivre le contenu du fichier lu, pour que celui-ci soit conforme à la liste présentée dans la partie précédente. On contrôle de plus que le contenu du fichier est conforme avec les paramètres de la partie, afin d'éviter que l'utilisateur ne modifie la sauvegarde de manière incohérente. 
\medbreak
\par A chaque début de partie, on contrôle l'existence d'un \texttt{sauvegarde.txt} dans le répertoire courant. Si celui-ci existe bien, on propose alors à l'utilisateur de charger les données contenues dans le fichier. Si la réponse est positive, on lit alors le fichier et on charge la partie correspondante, permettant au joueur de charger sa sauvegarde. Si l'utilisateur refuse, une nouvelle partie est commencée de la manière habituelle.


\section{Conclusion}
\par \emph{Flood-it} est au final un jeu qui se décline en deux versions : une version sur terminal \texttt{UNIX}, l'autre version est dotée d'une interface graphique. Chacune possède ses avantages et ses inconvénients, mais les deux possèdent les mêmes \emph{features}. La version sur terminal a l'avantage d'être rapide et portable, notamment entre les distributions \texttt{Linux}, mais sa présentation est austère et guère accueillante. La version graphique est plus intuitive, plus agréable, mais son installation nécessite la possession des paquets \texttt{SDL2} et \texttt{SDL2-ttf}.
\medbreak
\par Hormis l'optimisation du solveur, le jeu devrait répondre à toutes les contraintes et propose même des fonctionnalités supplémentaires dont le but est de rendre le jeu plus plaisant. Malheureusement, dû à un manque de temps, nous n'avons pas pu effectuer toutes les améliorations que nous aurions souhaîté apporter et rendre l'interface graphique plus belle et propre.
\medbreak
\par Néanmoins, ce projet nous a permis une première approche des méthodes de travail agiles ainsi que l'utilisation d'outils collaboratifs tels que \texttt{git}.

\end{document}