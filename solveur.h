/**
 * @file solveur.h
 * @author Damien BERNEAUX
 * @author Kevin COCCHI
 * @author Félix GOUEDARD
 * @author Van Man NGUYEN
 * @brief Interface du solveur.
 */

#ifndef SOLVEUR
#define SOLVEUR

#include <stdlib.h>

#include "piles.h"
#include "grille.h"

void solveur(grille, pile*, int, int);

#endif
