/**
 * @file game.c
 * @author Damien BERNEAUX
 * @author Kevin COCCHI
 * @author Félix GOUEDARD
 * @author Van Man NGUYEN
 * @brief Implémentation de game.h
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/times.h>
#include <termios.h>
#include <unistd.h>
#include <ctype.h>

#include "game.h"
#include "grille.h"
#include "piles.h"
#include "solveur.h"


/**
 * @fn int check_victoire(grille g, int tour, int max_tours)
 * @param g : la grille de jeu.
 * @param nb_tours : le nombre de tours déjà joués.
 * @param nb_tours_maxi : le nombre maximal de tours pour cette partie.
 * @return 0 si le joueur a gagné, 1 si la partie continue et 2 si le joueur a perdu.
 * @brief Détermine à chaque tour si le jeu continue ou s'arrête.
 */
int check_victoire(grille g, int nb_tours, int nb_tours_maxi) {
    if(onecolor_grille(g) == 1)
        return 0;
    else {
        if(nb_tours == nb_tours_maxi)
            return 2;
        else
            return 1;
    }
}


/**
 * @fn int read_key()
 * @return Valeur ASCII de la touche appuyée.
 * @brief Lit la touche appuyée au clavier sans passer par une confirmation via Entrée.
 */
int read_key() {
    struct termios oldattr, newattr;
    int ch;
    tcgetattr(STDIN_FILENO, &oldattr);
    newattr = oldattr;
    newattr.c_lflag &= ~( ICANON | ECHO );
    tcsetattr(STDIN_FILENO, TCSANOW, &newattr);
    ch = getchar();
    tcsetattr(STDIN_FILENO, TCSANOW, &oldattr);

    return ch;
}


/**
 * @fn int set_grid_size()
 * @return Taille choisie pour la grille.
 * @brief Permet à l'utilisateur de choisir entre les trois tailles de grille définies dans le cahier des charges.
 */
int set_grid_size() {
    int size = 0;

    printf("Veuillez choisir la taille de la grille :\n");
    printf("[q] : 12x12\n");
    printf("[s] : 18x18\n");
    printf("[d] : 24x24\n");
    printf("\n");

    while(size == 0) {
        char c = ' ';

        printf("Votre choix : ");
        c = read_key(); // On va lire le caractère au clavier.

        switch(c) {
        case 'q':
            size = 12;
            break;
        case 's':
            size = 18;
            break;
        case 'd':
            size = 24;
        default: // La bonne touche n'a pas été appuyée
            printf("\nVeuillez appuyer sur [q], [s] ou [d].\n");
        }
    }

    printf("\n\nVous avez choisi une grille de taille %dx%d.\n\n", size, size);

    return size;
}


/**
 * @fn int set_difficulty()
 * @return Choix de la difficulté (et donc du nombre de couleurs).
 * @brief Permet à l'utilisateur de choisir entre les trois difficultés (nombre de couleurs jouables).
 */
int set_difficulty() {
    int nb_couleurs = 0;

    printf("Veuillez choisir la difficulté :\n");
    printf("[q] : Facile (4 couleurs)\n");
    printf("[s] : Moyenne (6 couleurs)\n");
    printf("[d] : Difficile (8 couleurs)\n");
    printf("\n");

    while(nb_couleurs == 0) {
        char c = ' ';

        printf("Votre choix : ");
        c = read_key(); // On va lire le caractère au clavier.

        switch(c) {
        case 'q':
            nb_couleurs = 4;
            break;
        case 's':
            nb_couleurs = 6;
            break;
        case 'd':
            nb_couleurs = 8;
        default: // La bonne touche n'a pas été appuyée
            printf("\nVeuillez appuyer sur [q], [s] ou [d].\n");
        }
    }

    printf("\n\nVous avez choisi %d couleurs jouables.\n\n", nb_couleurs);

    return nb_couleurs;
}


/**
 * @fn select_color(int nb_couleurs)
 * @param nb_couleurs : le nombre de couleurs jouables.
 * @return Le caractère associé à la couleur choisie.
 * @brief Permet à l'utilisateur de sélectionner une couleur en la rentrant au clavier.
 */
char select_color(int nb_couleurs) {
    char c;
    char couleurs_to_string[] = "[B], [V], [R], [J], [M], [G], [P], [T]";
    couleurs_to_string[strlen(couleurs_to_string) - 5*(8-nb_couleurs)] = '\0';

    do {
        printf("Veuillez choisir une couleur parmi %s.\n", couleurs_to_string);
        printf("Votre couleur : ");
        c = toupper(read_key());
        printf("\n");
    } while(!couleur_valide(c, nb_couleurs));

    return c;
}


/**
 * @fn affichage_jeu(grille g, int nb_tours, int nb_tours_maxi)
 * @param g : la grille de jeu.
 * @param nb_tours : le nombre de tours déjà joués.
 * @param nb_tours_maxi : le nombre maximal de tours pour cette partie.
 * @brief Permet d'afficher la grille de jeu et le nombre de coups restants.
 */
void affichage_jeu(grille g, int nb_tours, int nb_tours_maxi) {
    int i;

    printf("\n");
    for(i = 0; i < g.size*2+1; i++)
        printf("=");

    printf(" Coups : %i/%i\n\n", nb_tours, nb_tours_maxi);

    affichage_grille(g);
    printf("\n");
}


/**
 * @fn void game_loop()
 * @brief Boucle principale de jeu.
 */
void game_loop() {
    int continuer = 1;
    int grille_init = 0;

    grille g; // Grille de jeu
    int N = 0; // Taille de la grille à créer
    int nb_couleurs_choisies = 6; // Nombre de couleurs jouables
    int nb_tours = 0; // Nombre de tours
    int nb_tours_maxi = 0; // Nombre de coups maximal

    char ancienne_couleur = 0; // Couleur sélectionnée sur le tour précédent
    char couleur_selectionnee = 0; // Couleur sélectionnée par l'utilisateur sur ce tour

    /* ---------------------------------------------------------------------- */

    FILE* fp = fopen("sauvegarde.txt", "r");
    if(fp != NULL) { // Un fichier de sauvargarde a été trouvé !
        printf("Un fichier de sauvegarde a été trouvé. Voulez-vous continuer (Y/N) ?\n");

        if(toupper(read_key()) == 'Y') {  // Le joueur veut continuer sa partie
            g = file_grille("sauvegarde.txt", &nb_tours, &nb_tours_maxi, &nb_couleurs_choisies);
            N = g.size;
            grille_init = 1;
        }
        fclose(fp);
    }

    if(!grille_init) { // Si aucune grille n'a encore été générée
        N = set_grid_size(); // Taille de la grille sélectionnée par l'utilisateur
        nb_couleurs_choisies = set_difficulty(); // Nombre de couleurs choisies par l'utilisateur
        g = random_grille(N, nb_couleurs_choisies); // Création d'une grille aléatoire de taille N

        pile solution = NULL;
        solveur(g, &solution, 0, nb_couleurs_choisies);
        nb_tours_maxi = hauteur(solution); // Nombre de coups maximal
        detruire(&solution);
    }

    /* ---------------------------------------------------------------------- */

    /* Boucle principale du jeu */
    while(continuer == 1) {
        continuer = check_victoire(g, nb_tours, nb_tours_maxi); // Vérification de la victoire
        affichage_jeu(g, nb_tours, nb_tours_maxi); // Affichage du plateau et nombre de coups

        if(continuer != 1)
            break;

        // Sélection d'une couleur pour ce tour
        do {
            couleur_selectionnee = select_color(nb_couleurs_choisies);
        } while(couleur_selectionnee == ancienne_couleur && couleur_valide(couleur_selectionnee, nb_couleurs_choisies));
        ancienne_couleur = couleur_selectionnee; // On met à jour l'ancienne couleur

        change_connexe(g, couleur_selectionnee); // Application des transformations de la grille
        nb_tours++; // Un tour vient de passer
        save_grille(g, nb_tours, nb_tours_maxi, nb_couleurs_choisies); // Sauvegarde de la grille
    }

    if(continuer == 0){
        printf("Vous avez gagné en %d coups. Félicitations !\n\n", nb_tours);
        remove("sauvegarde.txt");
      }
    if(continuer == 2){
        printf("Vous avez perdu ...\n\n");
        remove("sauvegarde.txt");
      }

    dealloc_grille(&g);
}


int main(int argc, char** argv) {
    (void) argc;
    (void) argv;

    game_loop();
    return 0;
}
