# Présentation
`flood-it` est un jeu solo de type ColorFlood dont le but est de recouvrir une grille par une seule couleur dans un nombre limité de tours.

# Installation
1. Téléchargez l'archive et décompressez là dans le répertoire de votre choix.
2. Ouvrez un terminal dans ce répertoire.
3. [*Terminal*] Faites `make flood-it` pour installer le jeu en version terminal.
3. [*Graphique*] Faites `make flood-it-gui` pour installer le jeu en version graphique. **Requiert `SDL2`**.
4. [*Optionnel*] Faites `make flood-it-doc` pour regénérer la documentation qui se trouve dans `Documentation/html/index.html`. **Requiert `doxygen`**.
5. [*Optionnel*] Faites `make tests` pour compiler la batterie de tests unitaires puis `./tests` pour les lancer. **Requiert `CUnit`**.

_**Note :** la compilation sous Windows nécessite MinGW._

# Jouer (Version terminal)
Une fois l'exécutable compilé pour la version **terminal** du jeu, faites `./flood-it` dans le répertoire pour lancer le programme.

Avant toute partie, le programme vérifie qu'une sauvegarde n'existe pas déjà. Si c'est le cas, vous pouvez choisir de continuer la partie en cours, ou bien d'en commencer une nouvelle.

Lorsque le jeu sera lancé, appuyez sur une des trois touches proposées pour choisir la taille de la grille à générer pour cette partie (12\*12, 18\*18 ou 24\*24).

Vous pouvez ensuite choisir la difficulté de la partie, c'est à dire le nombre de couleurs qui sera proposé (4, 6 ou 8).

Vous pouvez alors jouer en appuyant sur les touches `R`, `V`, `B`, `G`, `M`, `J`, `P`, `T` pour remplir la grille. Le nombre de coups maximal est déterminé automatiquement par le programme.

# Jouer (Version graphique)
Une fois l'exécutable compilé pour la version **graphique** du jeu, faites `./flood-it-gui` dans le répertoire pour lancer le programme.

Avant toute partie, le programme vérifie qu'une sauvegarde n'existe pas déjà. Si c'est le cas, vous pouvez choisir de continuer la partie en cours, ou bien d'en commencer une nouvelle.

Lorsque le jeu sera lancé, vous devrez tout d'abord choisir la taille de la grille parmi les trois propositions affichées (12\*12, 18\*18 ou 24\*24).

Vous pouvez ensuite choisir la difficulté de la partie, c'est à dire le nombre de couleurs qui sera proposé (4, 6 ou 8).

Vous pouvez alors jouer en cliquant sur une case d'une certaine couleur dans la grille, ou en appuyant sur les touches `R`, `V`, `B`, `G`, `M`, `J`, `P`, `T` du clavier. Le nombre de coups maximal est déterminé automatiquement par le programme.

# Désinstallation
Faites `make clean` dans le répertoire du jeu afin de supprimer les exécutables et fichiers de compilation.
