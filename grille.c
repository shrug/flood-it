/**
 * @file grille.c
 * @author Damien BERNEAUX
 * @author Kevin COCCHI
 * @author Félix GOUEDARD
 * @author Van Man NGUYEN
 * @brief Implémentation de grille.h
 */

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <errno.h>

#include "grille.h"

char COLORS[8] = {'B', 'V', 'R', 'J', 'M', 'G', 'P', 'T'};  /*!< Tableau contenant les couleurs disponibles */
int NB_COLORS = 8;  /*!< Taille du tableau de couleurs */


/**
 * @fn grille alloc_grille(int n)
 * @param n : entier correspondant à la taille de la grille à allouer.
 * @return Grille de taille n.
 * @brief Crée une grille de n*n cases en allouant l'espace mémoire qui convient.
 * Par défaut, la couleur d'une case est '0', et on met à 0 l'indicateur connexe.
 */
grille alloc_grille(int n) {
    if(n <= 0) {
        fprintf(stderr, "Erreur : grille de taille négative ou nulle.\n");
        exit(1);
    }

    grille g;
    boxes c;
    int i, j;

    g.size = n;
    g.val = (boxes**) malloc(n*sizeof(boxes*));

    for(i = 0; i < n; i++) {
        g.val[i] = (boxes*) malloc(n*sizeof(boxes));
        for(j = 0; j < n; j++) {
            c = (boxes) malloc(sizeof(struct cases));
            c->color = '0';
            c->connexe = 0;
            g.val[i][j] = c;
        }
    }
    g.val[0][0]->connexe = 1;

    return g;
}


/**
 * @fn grille dealloc_grille(grille *pg)
 * @param *pg : pointeur vers la grille à libérer.
 * @return Le pointeur vers la grille libérée.
 * @brief Libère la mémoire associée à la grille pg.
 */
grille dealloc_grille(grille *pg) {
    int i, j;

    for(i = 0; i < pg->size; i++) {
        for(j = 0; j < pg->size; j++)
            free(pg->val[i][j]);
        free(pg->val[i]);
    }

    free(pg->val);
    return *pg;
}


/**
 * @fn grille random_grille(int n, int nb_couleurs)
 * @param n : entier correspondant à la taille de la grille à allouer.
 * @param nb_couleurs : nombre de couleurs à afficher dans la grille.
 * @return Grille de taille n générée aléatoirement.
 * @brief Crée de manière aléatoire une grille de taille n avec le nombre de couleurs choisi.
 */
grille random_grille(int n, int nb_couleurs) {
    grille g = alloc_grille(n); // Création de la grille

    srand(time(NULL)); // Make random great again
    int i, j;

    for(i = 0; i < n; i++)
        for(j = 0; j < n; j++)
            g.val[i][j]->color = COLORS[rand() % nb_couleurs]; // Choix d'une couleur aléatoire

    identify_composante(g);
    return g;
}


/**
 * @fn grille file_grille(char* path, int* nb_tours, int* nb_tours_maxi, int* nb_couleurs)
 * @param path : le chemin du fichier à lire.
 * @param nb_tours : un pointeur vers le nombre de tours.
 * @param nb_tours_maxi : un pointeur vers le nombre de coups maximal.
 * @param nb_couleurs : un pointeur vers le nombre de couleurs jouables.
 * @return Grille générée à partir d'un fichier.
 * @brief Génère une grille à partir d'un fichier et mets à jour les variables de la partie.
 */
grille file_grille(char* path, int* nb_tours, int* nb_tours_maxi, int* nb_couleurs) {
    FILE* fp = NULL;

    // Ouverture du fichier
    fp = fopen(path, "r");
    if(fp == NULL) {
        fprintf(stderr, "Erreur lors de l'ouverture du fichier: %s\n", strerror(errno));
        exit(1);
    }

    // Lecture de la taille de la grille à créer
    char line[5];
    fgets(line, sizeof(line), fp);
    int n = atoi(line);

    //Récupération du nombre de couleurs
    line[0] = '\0';
    fgets(line, sizeof(line), fp);
    *nb_couleurs = atoi(line);

    //Récupération du nombre de tours joués
    line[0] = '\0';
    fgets(line, sizeof(line), fp);
    *nb_tours = atoi(line);

    //Récupération du nombre de tours max_tours
    line[0] = '\0';
    fgets(line, sizeof(line), fp);
    *nb_tours_maxi = atoi(line);
    line[0] = '\0';

    grille g = alloc_grille(n); // Création de la grille

    char buf[2]; // Buffer pour la lecture du caractère
    int i = 0, j = 0;

    // Lecture d'un caractère tant qu'on n'est pas à la fin
    while(fgets(buf, sizeof(buf), fp) != NULL) {

        switch(buf[0]) {
        case '\n':
            i++;

            if(i >= n) { // Vérification du nombre de lignes
                fprintf(stderr, "Dimensions invalides : n = %d, i = %d.\n", n, i);
                dealloc_grille(&g); // Échec de la création, libérer la mémoire avant de quitter
                exit(1);
            }
            if(j != n) { // Vérification du nombre d'éléments dans la ligne
                fprintf(stderr, "Dimensions invalides : %d;%d.\n", i, j);
                dealloc_grille(&g);
                exit(1);
            }
            j = 0;
            break;

        case ' ':
            break;

        case 'B':
        case 'V':
        case 'R':
        case 'J':
        case 'M':
        case 'G':
        case 'P':
        case 'T':
            if(j >= n) { // Vérifications de dimensions
                fprintf(stderr, "Dimensions invalides : %d;%d.\n", i, j);
                dealloc_grille(&g);
                exit(1);
            }
            if(!couleur_valide(buf[0], *nb_couleurs)) { // Vérification que cette couleur est jouable
                fprintf(stderr, "Erreur : couleur invalide : %c, (%d;%d)\n", buf[0], i, j);
                dealloc_grille(&g);
                exit(1);
            }
            change_color(g, i, j, buf[0]);
            j++;
            break;

        default:
            fprintf(stderr, "Erreur : couleur invalide : %c, (%d;%d)\n", buf[0], i, j);
            dealloc_grille(&g);
            exit(1);
        }
    }

    // On doit vérifier que la boucle ne s'est pas terminée à cause d'une erreur
    if(!feof(fp)) {
        fprintf(stderr, "Erreur, fin de lecture prématurée: %d;%d\n", i, j);
        dealloc_grille(&g);
        exit(1);
    }
    if(i < n-1) { // Vérification du nombre de lignes
        fprintf(stderr, "Dimensions invalides : n = %d, i = %d\n", n, i);
        dealloc_grille(&g); // Échec de la création, libérer la mémoire avant de quitter
        exit(1);
    }

    fclose(fp);

    identify_composante(g);
    return g;
}


/**
 * @fn void change_color(grille g, int x, int y, char c)
 * @param g : la grille de jeu.
 * @param x : ligne de la case à modifier.
 * @param y : colonne de la case à modifier.
 * @param c : nouvelle couleur de la case.
 * @brief Modifie la couleur de la case (x,y).
 */
void change_color(grille g, int x, int y, char c) {
    g.val[x][y]->color = c;
}


/**
 * @fn int onecolor_grille(grille g)
 * @param g : la grille de jeu.
 * @return 1 si toutes les cases de la grille sont de la même couleur, 0 sinon.
 * @brief Teste si toutes les cases de la grille sont de la même couleur.
 */
int onecolor_grille(grille g) {
    int i, j;
    char c = g.val[0][0]->color;

    for(i = 0; i < g.size; i++)
        for(j = 0; j < g.size; j++)
            if(g.val[i][j]->color != c)
                return 0;

    return 1;
}


/**
 * @fn void affichage_grille(grille g)
 * @param g : la grille de jeu.
 * @brief Affiche la grille.
 */
void affichage_grille(grille g) {
    int i, j;

    for(i = 0; i < g.size; i++) {
        printf(" ");
        for(j = 0; j < g.size; j++)
            printf("%c ", g.val[i][j]->color);
        printf("\n");
    }
}


/**
 * @fn void affichage_connexe(grille g)
 * @param g : la grille de jeu.
 * @brief Affiche la composante connexe de la grille.
 */
void affichage_connexe(grille g) {
    int i, j;

    for(i = 0; i < g.size; i++) {
        printf(" ");
        for(j = 0; j < g.size; j++)
            printf("%d ", g.val[i][j]->connexe);
        printf("\n");
    }
}


/**
 * @fn void identify_composante(grille g)
 * @param g : la grille de jeu.
 * @brief Identification de la composante connexe de la grille.
 */
void identify_composante(grille g) {
    int i, j;
    char couleur = g.val[0][0]->color; // Couleur de la composante connexe

    for(i = 0; i < g.size; i++) {
        for(j = 0; j < g.size; j++) {
            if(g.val[i][j]->connexe == 1) { // La case est dans la composante connexe
                if(i < (g.size - 1) && g.val[i+1][j]->color == couleur) // Case à droite ajoutée
                    g.val[i+1][j]->connexe = 1;
                if(i > 0 && g.val[i-1][j]->color == couleur) // Case à gauche ajoutée
                    g.val[i-1][j]->connexe = 1;
                if(j < (g.size - 1) && g.val[i][j+1]->color == couleur) // Case au dessus ajoutée
                    g.val[i][j+1]->connexe = 1;
                if(j > 0 && g.val[i][j-1]->color == couleur) // Case au dessous ajoutée
                    g.val[i][j-1]->connexe = 1;
            }
        }
    }
}


/**
 * @fn void change_connexe(grille g, char c)
 * @param g : la grille de jeu.
 * @param c : la couleur de remplacement.
 * @brief Change la couleur des cases de la composante connexe en la couleur souhaitée.
 */
void change_connexe(grille g, char c) {
    int i, j;

    for(i = 0; i < g.size; i++)
        for(j = 0; j < g.size; j++)
            if(g.val[i][j]->connexe == 1)
                change_color(g, i, j, c);

    identify_composante(g); // On identifie la composante connexe à nouveau
}


/**
 * @fn int couleur_valide(char c, int nb_couleurs_choisies)
 * @param c : la couleur dont il faut vérifier qu'elle est utilisable.
 * @param nb_couleurs_choisies : le nombre de couleurs jouables.
 * @return 1 si c est une couleur valide, 0 sinon.
 * @brief Vérifie que la couleur est bien une couleur jouable.
 */
int couleur_valide(char c, int nb_couleurs_choisies) {
    int i;

    for(i = 0; i < nb_couleurs_choisies; i++)
        if(c == COLORS[i])
            return 1;

    return 0;
}


/**
 * @fn int equals_grilles(grille g1, grille g2)
 * @brief Test d'égalité de deux grilles.
 * @param g1 : première grille.
 * @param g2 : seconde grille.
 * @return 0 si les grilles n'ont pas les mêmes couleurs ou dimensions, 1 sinon.
 */
int equals_grilles(grille g1, grille g2) {
    int i, j;

    if(g1.size != g2.size)
        return 0;

    for(i = 0; i < g1.size; i++)
        for(j = 0; j < g2.size; j++)
            if(g1.val[i][j]->color != g2.val[i][j]->color)
                return 0;

    return 1;
}


/**
 * @fn int equals_connexe(grille g1, grille g2)
 * @brief Test d'égalité des composantes connexes de deux grilles.
 * @param g1 : première grille.
 * @param g2 : seconde grille.
 * @return 0 si les grilles n'ont pas la même composante connexe, 1 sinon.
 */
int equals_connexe(grille g1, grille g2) {
    int i, j;

    if(g1.size != g2.size)
        return 0;

    for(i = 0; i < g1.size; i++)
        for(j = 0; j < g2.size; j++)
            if(g1.val[i][j]->connexe != g2.val[i][j]->connexe)
                return 0;

    return 1;
}


/**
 * @fn void save_grille(grille g, int n_coups, int n_coups_max, int n_couleur)
 * @param g : la grille de jeu.
 * @param n_coups : le nombre de coups joués.
 * @param n_coups_max : le nombre de coups maximum.
 * @param n_couleur : le nombre de couleurs jouables dans la partie.
 * @brief Sauvegarde des informations de la partie dans un ficher texte.
 */
void save_grille(grille g, int n_coups, int n_coups_max, int n_couleur) {
    FILE *fp = NULL;
    int i, j;

    //Création ou réécriture du fichier de sauvegarde
    fp = fopen("sauvegarde.txt", "w");
    if(fp == NULL) {
        fprintf(stderr, "Erreur lors de l'ouverture du fichier: %s\n", strerror(errno));
        exit(1);
    }

    char buf[10];

    //Ecriture de la taille de la grille
    sprintf(buf, "%i\n", g.size);
    fwrite(buf, sizeof(char), strlen(buf), fp);

    //Ecriture du nombre de couleurs
    buf[0] = '\0';
    sprintf(buf, "%i\n", n_couleur);
    fwrite(buf, sizeof(char), strlen(buf), fp);

    //Ecriture du nombre de coups joués
    buf[0] = '\0';
    sprintf(buf, "%i\n", n_coups);
    fwrite(buf, sizeof(char), strlen(buf), fp);

    //Ecriture du nombre maximum de coups
    buf[0] = '\0';
    sprintf(buf, "%i\n", n_coups_max);
    fwrite(buf, sizeof(char), strlen(buf), fp);

    //Ecriture de la grille dans le fichier de sauvegarde
    for(i = 0; i < g.size; i++) {
        for(j = 0; j < g.size; j++) {
            buf[0] = g.val[i][j]->color;
            fwrite(buf, sizeof(char), 1, fp);
        }
        if(i != g.size-1) {
            buf[0] = '\n';
            fwrite(buf, sizeof(char), 1, fp);
        }
    }

    fclose(fp);
}
