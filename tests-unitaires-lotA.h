/**
 * @file tests-unitaires-lotA.h
 * @author Damien BERNEAUX
 * @author Kevin COCCHI
 * @author Félix GOUEDARD
 * @author Van Man NGUYEN
 * @brief Interface des fonctions de tests unitaires
 */

#ifndef TESTS
#define TESTS

#include "grille.h"

void test_equals_grilles_vide(void);
void test_change_color(void);
void test_onecolor_grille(void);
void test_equals_grilles_file(void);
void test_change_connexe(void);
int test_all();

#endif
