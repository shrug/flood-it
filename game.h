/**
 * @file game.h
 * @author Damien BERNEAUX
 * @author Kevin COCCHI
 * @author Félix GOUEDARD
 * @author Van Man NGUYEN
 * @brief Interface des fonctions relatives au fonctionnement du jeu en terminal.
 */

#ifndef GAME
#define GAME

#include "grille.h"

int check_victoire(grille, int, int);

int read_key();

int set_grid_size();
int set_difficulty();
char select_color(int);

void affichage_jeu(grille, int, int);

void game_loop();

#endif
