/**
 * @file piles.h
 * @author Damien BERNEAUX
 * @author Kevin COCCHI
 * @author Félix GOUEDARD
 * @author Van Man NGUYEN
 * @brief Interface des fonctions relatives aux piles.
 */

#ifndef PILES
#define PILES

/**
 * @typedef pile
 * @brief pile désigne un pointeur vers un maillon.
 */
typedef struct maillon* pile;

/**
 * @struct maillon
 * @brief Un maillon est un élément d'une pile, il contient une couleur et pointe vers le maillon suivant.
 */
struct maillon {
    char couleur; /*!< Un caractère identifiant la couleur de la case */
    pile next; /*!< Pointeur vers le maillon suivant */
};

int est_vide(pile);

void detruire(pile*);

void empiler(char, pile*);
char depiler(pile*);

char sommet(pile);
int hauteur(pile);

void affiche_pile(pile);

#endif
