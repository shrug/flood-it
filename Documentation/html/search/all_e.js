var searchData=
[
  ['save_5fgrille',['save_grille',['../grille_8c.html#abec368d2b4f82c637fcca876575f0c63',1,'save_grille(grille g, int n_coups, int n_coups_max, int n_couleur):&#160;grille.c'],['../grille_8h.html#a41ef2f745852ebf26aab85dbf5d18f69',1,'save_grille(grille, int, int, int):&#160;grille.c']]],
  ['select_5fcolor',['select_color',['../game_8c.html#a285911473b51c6a9302b1ce4432f09eb',1,'select_color(int nb_couleurs):&#160;game.c'],['../game_8h.html#a5bb8af331bda7c5947c6beffa677d4f2',1,'select_color(int):&#160;game.c']]],
  ['set_5fdifficulty',['set_difficulty',['../game_8c.html#a5c18f93a6f6cca383985cc73953dbe37',1,'set_difficulty():&#160;game.c'],['../game_8h.html#a5c18f93a6f6cca383985cc73953dbe37',1,'set_difficulty():&#160;game.c']]],
  ['set_5fgrid_5fsize',['set_grid_size',['../game_8c.html#a8a0b00d60f618d3ffd6d057c203d8716',1,'set_grid_size():&#160;game.c'],['../game_8h.html#a8a0b00d60f618d3ffd6d057c203d8716',1,'set_grid_size():&#160;game.c']]],
  ['set_5frectangle_5fcolor',['set_rectangle_color',['../game-gui_8c.html#a67911c7741b53abf222adb02a98e8d3f',1,'set_rectangle_color(SDL_Renderer *renderer, SDL_Rect *rect, int r, int g, int b, int a):&#160;game-gui.c'],['../game-gui_8h.html#a14badb73417da62a9b6ef96918592bac',1,'set_rectangle_color(SDL_Renderer *, SDL_Rect *, int, int, int, int):&#160;game-gui.c']]],
  ['size',['size',['../structtableau.html#aa8a929caa1c73c416010b56ffa3498c3',1,'tableau']]],
  ['solveur',['solveur',['../solveur_8c.html#a72f04c00d00b03c67643475d99dca98c',1,'solveur(grille g, pile *solution, int profondeur, int nb_couleurs):&#160;solveur.c'],['../solveur_8h.html#ad59090ac9ecec1172bd6a632e63e2812',1,'solveur(grille, pile *, int, int):&#160;solveur.c']]],
  ['solveur_2ec',['solveur.c',['../solveur_8c.html',1,'']]],
  ['solveur_2eh',['solveur.h',['../solveur_8h.html',1,'']]],
  ['sommet',['sommet',['../piles_8h.html#a352ffcb7724ab4bee842905e1762d7d3',1,'piles.c']]]
];
