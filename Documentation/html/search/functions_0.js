var searchData=
[
  ['affichage_5fconnexe',['affichage_connexe',['../grille_8c.html#ac1db8b3e008ce04eaf940fc53f8e24b1',1,'affichage_connexe(grille g):&#160;grille.c'],['../grille_8h.html#af5990d5c2eaf6fe4fdd819e23d530ec4',1,'affichage_connexe(grille):&#160;grille.c']]],
  ['affichage_5fcoups',['affichage_coups',['../game-gui_8c.html#ad8994edb545b2d5bda0eb2f0d25913e5',1,'affichage_coups(SDL_Renderer *renderer, TTF_Font *font, int nb_tours, int nb_tours_maxi):&#160;game-gui.c'],['../game-gui_8h.html#a5f479f9cabfd76142aeacd9e723c33ca',1,'affichage_coups(SDL_Renderer *, TTF_Font *, int, int):&#160;game-gui.c']]],
  ['affichage_5fgrille',['affichage_grille',['../grille_8c.html#a35bfafbe626e17528a842592c056adcc',1,'affichage_grille(grille g):&#160;grille.c'],['../grille_8h.html#a06426d0b94af55be86777f0fb72fdc49',1,'affichage_grille(grille):&#160;grille.c']]],
  ['affichage_5fjeu',['affichage_jeu',['../game-gui_8c.html#a9301acad55de08eec9fc36e55f9c5fcf',1,'affichage_jeu(SDL_Renderer *renderer, grille g):&#160;game-gui.c'],['../game-gui_8h.html#a9cd4d6c84ce0614e987f4a43422d556e',1,'affichage_jeu(SDL_Renderer *, grille):&#160;game-gui.c'],['../game_8c.html#aeee3c201d59d3ea86868b374f8d3eda1',1,'affichage_jeu(grille g, int nb_tours, int nb_tours_maxi):&#160;game.c'],['../game_8h.html#a9c7ba8dae9929c5e666d6d48fe1f54c3',1,'affichage_jeu(grille, int, int):&#160;game.c']]],
  ['affiche_5fpile',['affiche_pile',['../piles_8h.html#a62453bcdcb06045a49d306cfe77e6012',1,'piles.c']]],
  ['alloc_5fgrille',['alloc_grille',['../grille_8c.html#ab70a921ad18d7330a9ee19915027eaf6',1,'alloc_grille(int n):&#160;grille.c'],['../grille_8h.html#a4fa4c111bb9af4c20a3826bd4844615f',1,'alloc_grille(int):&#160;grille.c']]],
  ['ask_5fload_5ffile',['ask_load_file',['../game-gui_8c.html#ab6a234ea114298b9bd26ea2cb9a68872',1,'ask_load_file(SDL_Renderer *renderer):&#160;game-gui.c'],['../game-gui_8h.html#a7fe6783988a19a317ad35d2e3c35ef62',1,'ask_load_file(SDL_Renderer *):&#160;game-gui.c']]]
];
