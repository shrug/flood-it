var searchData=
[
  ['case_5fsize',['CASE_SIZE',['../game-gui_8c.html#ae5e96d702886a38dfbc314cbda91e437',1,'game-gui.c']]],
  ['cases',['cases',['../structcases.html',1,'']]],
  ['change_5fcolor',['change_color',['../grille_8c.html#a1372a2cdaaaf24e5660e2c1496a8c422',1,'change_color(grille g, int x, int y, char c):&#160;grille.c'],['../grille_8h.html#a0bb2f3d9663f3c9da066c941a878ff49',1,'change_color(grille, int, int, char):&#160;grille.c']]],
  ['change_5fconnexe',['change_connexe',['../grille_8c.html#a020f0d176533f3f5e6a277d3b07e3c59',1,'change_connexe(grille g, char c):&#160;grille.c'],['../grille_8h.html#a4fbbe3618844e9803edfe8182fdcdaca',1,'change_connexe(grille, char):&#160;grille.c']]],
  ['change_5fconnexe_5flegacy',['change_connexe_legacy',['../tests-unitaires-lotA_8c.html#aa75118f25712f5c656cb3c4377569765',1,'tests-unitaires-lotA.c']]],
  ['check_5fvictoire',['check_victoire',['../game-gui_8c.html#aaec395856ba53fc16a7d91f7a737754e',1,'check_victoire(grille g, int nb_tours, int nb_tours_maxi):&#160;game-gui.c'],['../game-gui_8h.html#a602dcba1ab0abe1d99eef15a02111e6b',1,'check_victoire(grille, int, int):&#160;game-gui.c'],['../game_8c.html#aaec395856ba53fc16a7d91f7a737754e',1,'check_victoire(grille g, int nb_tours, int nb_tours_maxi):&#160;game.c'],['../game_8h.html#a602dcba1ab0abe1d99eef15a02111e6b',1,'check_victoire(grille, int, int):&#160;game-gui.c']]],
  ['color',['color',['../structcases.html#ac8d9e6dd767234b99beb1ee0f832653f',1,'cases']]],
  ['colors',['COLORS',['../grille_8c.html#a5279f71dcd3a6083017f5fc813b3b7da',1,'grille.c']]],
  ['connexe',['connexe',['../structcases.html#ad6d5df328e50f380a131ac85c53d99fd',1,'cases']]],
  ['couleur',['couleur',['../structmaillon.html#a8cdcbefeb249ca742b93e14e4097bfcc',1,'maillon']]],
  ['couleur_5fvalide',['couleur_valide',['../grille_8c.html#abb56fdedeb068bfdf1d3af4c4a6bd8d2',1,'couleur_valide(char c, int nb_couleurs_choisies):&#160;grille.c'],['../grille_8h.html#a676fe2de6104c3c58941dcb95d26e6d2',1,'couleur_valide(char, int):&#160;grille.c']]],
  ['create_5frectangle',['create_rectangle',['../game-gui_8c.html#aad323c3cad56439e6133a4b3f846a2b3',1,'create_rectangle(int x, int y, int w, int h):&#160;game-gui.c'],['../game-gui_8h.html#ac0af4ac99edd908e93ee6f4f8f5327bb',1,'create_rectangle(int, int, int, int):&#160;game-gui.c']]]
];
