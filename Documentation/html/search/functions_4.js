var searchData=
[
  ['file_5fgrille',['file_grille',['../grille_8c.html#ab3b6e566b97a3b41ee95f09c4ed6aa19',1,'file_grille(char *path, int *nb_tours, int *nb_tours_maxi, int *nb_couleurs):&#160;grille.c'],['../grille_8h.html#a0e072353df8ed157eb85e58fff57e672',1,'file_grille(char *, int *, int *, int *):&#160;grille.c']]],
  ['file_5fgrille_5flegacy',['file_grille_legacy',['../tests-unitaires-lotA_8c.html#a31013ef171f2d12ba2ed67a1ba32fb20',1,'tests-unitaires-lotA.c']]],
  ['fill_5fscreen',['fill_screen',['../game-gui_8c.html#ae2ea19fd123e5c2593b2d35d62ca67c5',1,'fill_screen(SDL_Renderer *renderer, int r, int g, int b, int a):&#160;game-gui.c'],['../game-gui_8h.html#af25e6a7c865011f4d2d6489d158f8674',1,'fill_screen(SDL_Renderer *, int, int, int, int):&#160;game-gui.c']]]
];
