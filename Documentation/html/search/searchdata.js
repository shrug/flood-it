var indexSectionsWithContent =
{
  0: "abcdefghimnoprstv",
  1: "cmt",
  2: "gpst",
  3: "acdefghioprst",
  4: "cdnsv",
  5: "bgp",
  6: "p"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "pages"
};

var indexSectionLabels =
{
  0: "Tout",
  1: "Structures de données",
  2: "Fichiers",
  3: "Fonctions",
  4: "Variables",
  5: "Définitions de type",
  6: "Pages"
};

