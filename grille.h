/**
 * @file grille.h
 * @author Damien BERNEAUX
 * @author Kevin COCCHI
 * @author Félix GOUEDARD
 * @author Van Man NGUYEN
 * @brief Interface des fonctions relatives à la manipulation des grilles.
 */

#ifndef GRID
#define GRID

/**
 * @struct cases
 * @brief Définition de la structure de case.
 * La structure est composée d'un char indiquant la couleur contenue, et d'un int indiquant son appartenance ou non à la composante connexe.
 */
typedef struct cases* boxes;

/**
 * @typedef boxes
 * @brief boxes désigne un pointeur vers une case.
 */
struct cases {
    char color; /*!< Un caractère identifiant la couleur de la case */
    int connexe; /*!< Un entier valant 1 si la case appartient à la composante connexe, 0 sinon */
};


/**
 * @struct tableau
 * @brief Définition de la structure de tableau.
 * Le tableau défini est un carré de boxes, composée d'un entier indiquant son côté et d'une matrice de boxes.
 */
typedef struct tableau grille;

/**
 * @typedef grille
 * @brief On désigne par "grille" un struct tableau.
 */
struct tableau {
    int size; /*!< Entier donnant la taille de la grille */
    boxes** val; /*!< Pointeur vers un pointeur de boxes, matrice de taille n*n cases */
};


grille alloc_grille(int);
grille dealloc_grille(grille*);

grille random_grille(int, int);
grille file_grille(char*, int*, int*, int*);

void change_color(grille, int, int, char);
int onecolor_grille(grille);

void affichage_grille(grille);
void affichage_connexe(grille);

void identify_composante(grille);
void change_connexe(grille, char);

int couleur_valide(char, int);

int equals_grilles(grille, grille);
int equals_connexe(grille, grille);

void save_grille(grille, int, int, int);

#endif
