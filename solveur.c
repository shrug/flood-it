/**
 * @file solveur.c
 * @author Damien BERNEAUX
 * @author Kevin COCCHI
 * @author Félix GOUEDARD
 * @author Van Man NGUYEN
 * @brief Implémentation du solveur.
 */

#include <stdlib.h>
#include <stdio.h>

#include "grille.h"
#include "piles.h"


/**
  * @fn void solveur(grille g, pile *solution, int profondeur, int nb_couleurs)
  * @param g : la grille de jeu.
  * @param solution : pointeur vers la pile qui contiendra la solution.
  * @param profondeur : indique la profondeur actuelle.
  * @param nb_couleurs : nombre de couleurs jouables dans la grille.
  * @brief Fonction récursive. Modifie par référence la pile solution en y stockant les coups nécessaires pour arriver à une solution de la grille.
  */
void solveur(grille g, pile *solution, int profondeur, int nb_couleurs) {
    char COLORS[8] = {'B', 'V', 'R', 'J', 'M', 'G', 'P', 'T'};
    int i, j, k;
    grille gCopie = alloc_grille(g.size);

    for(k = 0; k < nb_couleurs; k++) {
        for(i = 0; i < g.size; i++) {
            for(j = 0; j < g.size; j++) {
                gCopie.val[i][j]->color = g.val[i][j]->color;
                gCopie.val[i][j]->connexe = g.val[i][j]->connexe;
            }
        }

        if(COLORS[k] != g.val[0][0]->color) {
            empiler(COLORS[k], solution);
            change_connexe(gCopie, COLORS[k]);

            if(!equals_connexe(g, gCopie)) { // Si la composante a changée

                if(onecolor_grille(gCopie)) { // Si la grile n'est que d'une seule couleur, fin
                    dealloc_grille(&gCopie);
                    return;
                }
                else {
                    solveur(gCopie, solution, profondeur + 1, nb_couleurs);
                    dealloc_grille(&gCopie);
                    return;
                }

            }

            depiler(solution);
        }
    }

    dealloc_grille(&gCopie);
    return;
}
