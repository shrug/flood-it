all: flood-it

# Gestion des piles
piles.o: piles.c piles.h
	gcc -Wall -Wextra -c $<

# Gestion de la grille de jeu
grille.o: grille.c grille.h
	gcc -Wall -Wextra -c $<

# Solveur
solveur.o: solveur.c solveur.h grille.h piles.h
	gcc -Wall -Wextra -c $<

# Tests unitaires sur les grilles de jeu
tests.o: tests-unitaires-lotA.c tests-unitaires-lotA.h grille.h
	gcc -Wall -Wextra -o tests.o -c $< -lcunit

tests: tests.o grille.o
	gcc -Wall -Wextra $^ -o $@ -lcunit

# Jeu en version Terminal
game.o: game.c game.h grille.h piles.h solveur.h
	gcc -Wall -Wextra -o game.o -c $<

flood-it: grille.o piles.o solveur.o game.o
	gcc -Wall -Wextra $^ -o $@

# Jeu en version Graphique
game-gui.o: game-gui.c game-gui.h grille.h piles.h solveur.h
	gcc -Wall -Wextra -o game-gui.o -c $<

flood-it-gui: grille.o piles.o solveur.o game-gui.o
ifeq ($(OS),Windows_NT)
	gcc -Wall -Wextra $^ -o $@ -lmingw32 -lSDL2main -lSDL2 -lSDL2_ttf
else
	gcc -Wall -Wextra $^ -o $@ -lSDL2main -lSDL2 -lSDL2_ttf
endif

# Nettoyage des exécutables et fichiers temporaires
clean:
	rm *o flood-it flood-it-gui tests

# Génération de la documentation
flood-it-doc :
	doxygen grille_doc_config
