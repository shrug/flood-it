/**
 * @file game-gui.h
 * @author Damien BERNEAUX
 * @author Kevin COCCHI
 * @author Félix GOUEDARD
 * @author Van Man NGUYEN
 * @brief Interface des fonctions relatives au fonctionnement du jeu en graphique.
 */

#ifndef GAME_GUI
#define GAME_GUI

#include "grille.h"

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

int check_victoire(grille, int, int);

SDL_Rect create_rectangle(int, int, int, int);
void set_rectangle_color(SDL_Renderer*, SDL_Rect*, int, int, int, int);

void display_text(SDL_Renderer*, TTF_Font*, char*, int, int, int, int, int, int);
void fill_screen(SDL_Renderer*, int, int, int, int);

int ask_load_file(SDL_Renderer*);
int pick_grid_size(SDL_Renderer*);
int pick_difficulty(SDL_Renderer*);

void affichage_jeu(SDL_Renderer*, grille);
void affichage_coups(SDL_Renderer*, TTF_Font*, int, int);

char get_color(grille, int, int);

void game_loop();

#endif
